<div class="container">
    <div class="row">
        <div class="col-12 title">
            <h2>Conferma il tuo ordine</h2>
        </div>
    </div>
        <?php $items = $cart->getItems();
        $totale = 0;
        $i = 0;
        foreach ($items as $evento) :
            $j = 0;
            $e = $dbh->getEventById(array_keys($items)[$i])[0];
            $i++;
            foreach ($evento as $slottino) : ?>
                <input type="hidden" name="eventId" value="<?php echo array_keys($items)[$i];?>">
                <div class="card shadow my-4" id="<?php echo $event["eventCode"];?>">
                    <h5 class="card-header align-middle py-3">
                        <?php echo $e["title"] ?> - <?php echo array_keys($evento)[$j];?>
                    </h5>
                    <div class="card-body p-3">
                        <h5 class="card-title"><?php echo $e["userName"] ?></h5>
                        <div class="row justify-content-end">
                            <div class="col-10 text-left">
                                <p class="card-text"><?php echo $e["eventDate"] ?></br><?php echo $e["eventTime"] ?></p></br>
                            </div>
                            <div class="col-2 align-middle text-right p-0">
                                <p class="py-2 px-3"><?php echo $slottino["quantity"];?></p>
                                <ul class="dropdown-menu">
                                    <?php for($nq=0; $nq<10; $nq++):?>
                                        <li><a href="cart.php?action=3&eventId=<?php echo $e["eventCode"];?>&slot=<?php echo array_keys($evento)[$j];?>&quantity=<?php echo $nq?>"><?php echo $nq;?></a></li>
                                    <?php endfor;?>
                                </ul>
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-4 text-right">
                                <p class="py-0 my-0 h3"><?php echo $slottino["price"]; ?>€</p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $j++; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
        <hr>
        <div class="row">
            <div class="col-12 text-right">
                <b><h4>Totale: <p class="text-danger"><?php echo $cart->getTotalPrice();?>€</p></h4></b>
            </div>
        </div>
        <div class="row">
            <div class="col-6 text-left">
                <a href="cart.php" class="btn btn-outline-primary">Torna al carrello</a>
            </div>
            <div class="col-6 text-right">
                <a href="tickets.php?buy" class="btn btn-primary" <?php if(isset($_SESSION["soldout"])): echo "id='sold-out'"; endif;?>>Acquista</a>
            </div>
        </div>
</div>