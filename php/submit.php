<?php 
include 'bootstrap.php';

if(isset($_SESSION["user"]) && isset($_SESSION["dest"]) && isset($_SESSION["notifica"]) && isset($_SESSION["username"]))
{
    $criptokey = mysqli_real_escape_string($dbh->getConnection(), $_SESSION["notifica"]."-".$_SESSION["user"]."-".$_SESSION["dest"]);
    $sender = mysqli_real_escape_string($dbh->getConnection(), $_SESSION["username"]);
    $date = mysqli_real_escape_string($dbh->getConnection(), date("Y-m-d"));
    $time =mysqli_real_escape_string($dbh->getConnection(), date("H:i:s"));

    if($_SESSION["dest"]=="admin") {
        $admins = $dbh->allAdmins();
        
        foreach($admins as $admin):
            $name = mysqli_real_escape_string($dbh->getConnection(), $admin["username"]);
            $query = "INSERT INTO notification (criptokey, destadmin, senderusername, notificationread, notificationdate, notificationtime) VALUES ('$criptokey', '$name', '$sender', false, '$date', '$time')";
            mysqli_query($dbh->getConnection(), $query);
        endforeach;
    } else if($_SESSION["dest"]=="organizer") {
        if(isset($_SESSION["soldout"])) {
            $dest = mysqli_real_escape_string($dbh->getConnection(), $_SESSION["destusername"]);
            $eventcode = mysqli_real_escape_string($dbh->getConnection(), $_SESSION["soldout"]);
            $query = "INSERT INTO notification (criptokey, destorganizer, senderusername, notificationread, notificationdate, notificationtime, eventcode) VALUES ('$criptokey', '$dest', '$sender', false, '$date', '$time', '$eventcode')";
            mysqli_query($dbh->getConnection(), $query);
        } else {
            $dest = mysqli_real_escape_string($dbh->getConnection(), $_SESSION["destusername"]);
            $query = "INSERT INTO notification (criptokey, destorganizer, senderusername, notificationread, notificationdate, notificationtime) VALUES ('$criptokey', '$dest', '$sender', false, '$date', '$time')";
            mysqli_query($dbh->getConnection(), $query);
        }
    } else if($_SESSION["dest"]=="customer") {
        if(($_SESSION["notifica"]=="modifica" || $_SESSION["notifica"]=="elimina") && isset($_SESSION["eventoModificato"])) {
            $event = $_SESSION["eventoModificato"];
            $customers = $dbh->getCustomersOfEvent($event);
            foreach ($customers as $customer):
                $dest = mysqli_real_escape_string($dbh->getConnection(), $customer["username"]);
                $query = "INSERT INTO notification (criptokey, destcustomer, senderusername, notificationread, notificationdate, notificationtime, eventcode) VALUES ('$criptokey', '$dest', '$sender', false, '$date', '$time', '$event')";
                mysqli_query($dbh->getConnection(), $query);
            endforeach;
            
        } else {
            $dest = mysqli_real_escape_string($dbh->getConnection(), $_SESSION["destusername"]);
            $query = "INSERT INTO notification (criptokey, destcustomer, senderusername, notificationread, notificationdate, notificationtime) VALUES ('$criptokey', '$dest', '$sender', false, '$date', '$time')";
            mysqli_query($dbh->getConnection(), $query);
        }
    }
} else {
}

?>