<?php
require_once 'bootstrap.php';
$_SESSION["page"] = "login";

if(isset($_POST["username"]) && isset($_POST["password"])){
    $login = $dbh->checkLogin($_POST["username"], $_POST["password"]);
    if(!$login){
        $templateParams["errorelogin"] = "Errore! Controllare username o password!";
    }
}

if(isUserLoggedIn()){
    $templateParams["titolo"] = "e20 - User";
    $templateParams["nome"] = "login-data.php";
    if(isset($_POST["update-profile"])){
        $templateParams["nome"] = "login-update.php";
    }
    if(isset($_POST["update-password"])){
        $templateParams["nome"] = "login-update-password.php";
    }
    if(isset($_POST["leave-password"])){
        $templateParams["nome"] = "login-data.php";
    }
    if(isset($_POST["save-edits"])){
        $dbh->updateProfile($_POST["username"], $_POST["firstname"], $_POST["lastname"], $_POST["email"], $_POST["city"], $_POST["province"], $_POST["region"], $_POST["country"], $_POST["birthdate"], $_SESSION["username"]);
        $_SESSION["username"] = $_POST["username"];
        $templateParams["nome"] = "login-data.php";
    }
    if(isset($_POST["save-password"])){
        $templateParams["nome"] = "login-update-password.php";
        if(($_POST["oldpassword"]) == "" || ($_POST["newpassword"]) == ""){
            $templateParams["errorepassword"] = "Errore! Compilare tutti i campi!";
        } elseif($_POST["newpassword"] != $_POST["confirmpassword"]){
            $templateParams["errorepassword"] = "Errore! La nuova password non combacia!";
        } else{
            $password = $dbh->updatePassword($_POST["oldpassword"], $_POST["newpassword"], $_SESSION["username"]);
            if(!$password){
                $templateParams["errorepassword"] = "Errore! Controllare la vecchia password inserita!";
            } else{
                $templateParams["nome"] = "login-data.php";
            }
        }
    }
}
else{
    $templateParams["titolo"] = "e20 - Login";
    $templateParams["nome"] = "login-login.php";
}

require 'template/base.php';
?>
