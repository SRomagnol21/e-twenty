<?php
require_once 'bootstrap.php';

$templateParams["titolo"] = "e20 - Notifiche";
$_SESSION["page"] = "notification";

if(isset($_POST["notificationcode"])) {
    $_SESSION["notificationcode"]= $_POST["notificationcode"];
    $_SESSION["senderusername"] = $dbh->getNotificationSender($_SESSION["notificationcode"])[0]["senderusername"];
    $_SESSION["sendertype"] = $dbh->getUserType($_SESSION["senderusername"]);
    $cripto = $dbh->getCriptoByCode($_SESSION["notificationcode"])[0]["criptokey"];

if($cripto == "modifica-organizer-customer" || $cripto == "soldout-customer-organizer" || $cripto == "elimina-organizer-customer") {
    $eventcode = $dbh->getEventCodeByNotification($_SESSION["notificationcode"])[0]["eventCode"];
    $dbh->readNotification($_SESSION["notificationcode"]);
    header("location: event.php?eventcode=".$eventcode);
}
    
    if(isset($_POST["response"])) {

        $_SESSION["dest"] = $_SESSION["sendertype"];
        $_SESSION["destusername"] = $_SESSION["senderusername"];

        if($_POST["response"]=="confirmation") {
            $_SESSION["notifica"] = "confirmation";
        } else if($_POST["response"]=="denial") {
            $_SESSION["notifica"] = "denial";
        } else {
            
        }
    }
    
    if($_SESSION["user"]=="admin"){
        if($cripto=="subscription-organizer-admin") {  
            $templateParams["nome"] = "notification-confirm-organizer.php";
            $templateParams["senderdata"] = $dbh->getOrganizerData($dbh->getSenderByNotificationCode($_POST["notificationcode"])[0]["senderusername"]); 
        } else {
            
        }
    } else if($_SESSION["user"]=="organizer"){
        $templateParams["nome"] = "notification-for-organizer.php";
    } else if($_SESSION["user"]=="customer") {
        $templateParams["nome"] = "notification-for-customer.php";
    }
} else if(isset($_POST["response"])) {

    $_SESSION["dest"] = $_SESSION["sendertype"];
    $_SESSION["destusername"] = $_SESSION["senderusername"];
    $dbh->readNotification($_SESSION["notificationcode"]);

    if($_POST["response"]=="Conferma utente") {
        $_SESSION["notifica"] = "confirmation";
        $dbh->activeOrganizer($_SESSION["destusername"]);
    } else if($_POST["response"]=="Nega utente") {
        $_SESSION["notifica"] = "denial";
    } else {
        
    }

    $templateParams["nome"] = "notification-ok.php";
    

} else if(isset($_POST["ok"])) {
    $dbh->readNotification($_SESSION["notificationcode"]);
    $templateParams["nome"] = "notification-all.php";
} else {
    $templateParams["nome"] = "notification-all.php";
}

require 'template/base.php';
?>