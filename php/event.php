<?php
require_once 'bootstrap.php';
if(isset($_SESSION["soldout"])) {
    unset($_SESSION["soldout"]);
}

if(isset($_POST["eventcode"]) || isset($_GET["eventcode"])) {
    $eventcode = (isset($_POST["eventcode"]) ? $_POST["eventcode"] : $_GET["eventcode"]);
    $event = $dbh->getEventByIdAlsoDeleted($eventcode)[0];
    $slots = $dbh->getPriceSlotsByEventId($eventcode);
    $templateParams["nome"] = "event-home.php";
}

require 'template/base.php';
?>
