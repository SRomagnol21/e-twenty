<?php 
require_once 'bootstrap.php';
if(!isset($_SESSION["username"])) {
    $templateParams["nome"] = "cart-login.php";
} else {

    $templateParams["nome"] = "cart-home.php";
    
    if(isset($_GET["action"])){
        if($_GET["action"]=="1") {
            $templateParams["nome"] = "cart-order.php";
        } else if($_GET["action"]=="2") {
            $cart->clear();
        } else if($_GET["action"]=="3") {
            $slot = $dbh->getSlotCodeByName($_GET["eventId"], $_GET["slot"])[0];
            $sold = $dbh->getSoldTicketsForEventAndSlot($_GET["eventId"], $slot["slotCode"])[0]["COUNT(e.eventCode)"];
            $tosell = $slot["maxLimit"] - $sold;
            if($tosell >= $_GET["quantity"]) {
                $cart->update($_GET["eventId"], $_GET["slot"], $_GET["quantity"]);
            } else {
                $_SESSION["limit".$slot["slotCode"]] = "Per la posizione ".$_GET["slot"]." sono rimasti solo ".$tosell." biglietti.";
            }
        }
    }

    if(isset($_POST["eventcode"])) {
        $event = $dbh->getEventById($_POST["eventcode"])[0];
        $slots = $dbh->getPriceSlotsByEventId($_POST["eventcode"]);
        foreach($slots as $slot):
            if(isset($_POST["price".$slot["slotCode"]]) && isset($_POST["quantity".$slot["slotCode"]]) && $_POST["quantity".$slot["slotCode"]]>0) {
                $sold = $dbh->getSoldTicketsForEventAndSlot($event["eventCode"], $slot["slotCode"])[0]["COUNT(e.eventCode)"];
                $tosell = $slot["maxLimit"] - $sold;
                
                if($_POST["quantity".$slot["slotCode"]] > $tosell) {
                    $_SESSION["limit".$slot["slotCode"]] = "Per la posizione ".$slot["position"]." sono rimasti solo ".$tosell." biglietti.";
                    $cart->add($event["eventCode"], $_POST["slotRequested".$slot["slotCode"]], $tosell, $_POST["price".$slot["slotCode"]]);
                    $_SESSION["soldout"] = $event["eventCode"];
                } else {
                    $cart->add($event["eventCode"], $_POST["slotRequested".$slot["slotCode"]], $_POST["quantity".$slot["slotCode"]], $_POST["price".$slot["slotCode"]]);
                    if($_POST["quantity".$slot["slotCode"]] == $tosell) {
                        $_SESSION["soldout"] = $event["eventCode"];
                    }
                }
            }
        endforeach;
        unset($_POST["eventcode"]);
    }
}
require 'template/base.php';
?>