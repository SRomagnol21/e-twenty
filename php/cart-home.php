<div class="container">
    <div class="row">
        <div class="col-12 title">
            <h2>Il mio carrello</h2>
        </div>
    </div>
    <?php if($cart->isEmpty()):?>
        <div class="row mt-5" id="empty-cart">
            <div class="col-sm-4 offset-sm-4 text-center">
                <h4>Il tuo carrello è vuoto.</h4>
                <p>Vai alla <a href="index.php">Home</a> per esplorare gli eventi e aggiungerli al tuo carrello.</p>
            </div>
        </div>
    <?php else:?>
        <?php $items = $cart->getItems();
        $i = 0;
        foreach($items as $evento): 
            $j = 0;
            $e = $dbh->getEventById(array_keys($items)[$i])[0]; 
            $i++;
            foreach($evento as $slottino):?>
                <input type="hidden" name="eventId" value="<?php echo array_keys($items)[$i];?>">
                <div class="card shadow my-4" id="<?php echo $event["eventCode"];?>">
                    <h5 class="card-header align-middle py-3">
                        <?php echo $e["title"] ?> - <?php echo array_keys($evento)[$j];?>
                    </h5>
                    <div class="card-body p-3">
                        <h5 class="card-title"><?php echo $e["userName"] ?></h5>
                        <div class="row justify-content-end">
                            <div class="col-10 text-left">
                                <p class="card-text"><?php echo $e["eventDate"] ?></br><?php echo $e["eventTime"] ?></p></br>
                            </div>
                            <div class="col-2 align-middle text-right p-0">
                                <button class="btn dropdown-toggle py-2" data-toggle="dropdown"><?php echo $slottino["quantity"];?>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <?php for($nq=0; $nq<10; $nq++):?>
                                        <li><a href="cart.php?action=3&eventId=<?php echo $e["eventCode"];?>&slot=<?php echo array_keys($evento)[$j];?>&quantity=<?php echo $nq?>"><?php echo $nq;?></a></li>
                                    <?php endfor;?>
                                </ul>
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-4 text-right">
                                <p class="py-0 my-0 h3"><?php echo $slottino["price"]; ?>€</p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $j++;?>
            <?php endforeach; ?>
        <?php endforeach; ?>
        <hr>
        <?php if(isset($_SESSION["limit1"]) || isset($_SESSION["limit2"]) || isset($_SESSION["limit3"])):?>
            <?php if(isset($_SESSION["limit1"])): echo $_SESSION["limit1"]; unset($_SESSION["limit1"]); endif;?>
            <?php if(isset($_SESSION["limit2"])): echo $_SESSION["limit2"]; unset($_SESSION["limit2"]); endif;?>
            <?php if(isset($_SESSION["limit3"])): echo $_SESSION["limit3"]; unset($_SESSION["limit3"]); endif;?>
        <?php echo "<hr class='mt-2 mb-2'>"; endif;?>
        <div class="row">
            <div class="col-6">
                <a href="cart.php?action=2" class="btn btn-outline-primary">Svuota carrello</a>
            </div>
            <div class="col-6 text-right">
                <b><h4>Totale: <p class="text-danger"><?php echo $cart->getTotalPrice();?>€</p></h4></b>
            </div>
        </div>
        <div class="row">
            <div class="col-6 text-left">
                <a href="index.php" class="btn btn-outline-primary">Vedi altri eventi</a>
            </div>
            <div class="col-6 text-right">
                <a href="cart.php?action=1" class="btn btn-primary">Conferma acquisto</a>
            </div>
        </div>
    <?php endif;?>
</div>