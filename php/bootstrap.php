<?php
session_start();
if(PHP_OS == "Windows"){
    $templateParam["separator"] = "\\";
} else {
    $templateParam["separator"] = "/";
}
define("STYLE", "..".$templateParam["separator"]."css".$templateParam["separator"]."style.css");
//define("UPLOAD_DIR", $templateParam["separator"]."e20".$templateParam["separator"]."upload".$templateParam["separator"]);
define("UPLOAD_DIR", $templateParam["separator"]."e-twenty".$templateParam["separator"]."upload".$templateParam["separator"]);
require_once("database.php");
require_once("cart-class.php");
require_once("functions.php");
//$dbh = new DatabaseHelper("localhost", "root", "", "etwenty");
$dbh = new DatabaseHelper("localhost", "root", "", "e20");

if(isset($_SESSION["username"])) {
    $cart = new Cart(['maxItems' => 5, 'itemLimit'=> 10]);
}
?>
