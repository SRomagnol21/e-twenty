<?php
require_once "bootstrap.php";

if (isset($_POST["ticket"])) {
    
    $ticketcode = $_POST["ticket"];
    $ticket = $dbh->getTicket($ticketcode)[0];
    $event = $dbh->getEventById($ticket["eventcode"])[0];
    $slot = $dbh->getSlotByCode($ticket["eventcode"], $ticket["slotcode"])[0];
    $customer = $dbh->getCustomerData($ticket["username"])[0];

    $image = imagecreatetruecolor(1000, 1000);  
    $white = imagecolorallocate($image, 255, 255, 255);
    $black = imagecolorallocate($image, 0, 0, 0);
    
    imagefilledrectangle($image, 0, 0, 1000, 1000, $white);
    $logo = imagecreatefromjpeg("../img/logo3.jpg");
    imagecopymerge($image, $logo, 0, 0, 0, 0, 920, 370, 100);
    
    
    
    $txt = 
"        Evento: ".$event["title"]."
        Organizzatore: ".$event["userName"]."
        Luogo: ".$event["placeName"].", ".$event["city"]." (".$event["province"].") ".$event["region"].", ".$event["country"]."
        Data: ".$event["eventDate"]."
        Ora: ".$event["eventTime"]."
            
        Codice Biglietto: ".$ticket["ticketcode"]."
        Intestatario: ".$customer["firstname"]." ".$customer["lastname"]."
        Posizione: ".$slot["position"]."
        Prezzo: ".$slot["price"]."€";

    
    imagettftext($image, 20, 0, 30, 450, $black, "../font/Arial.ttf", $txt);
    
    header('Content-Type: image/jpeg');
    ob_start();
    imagejpeg($image);
    $img = ob_get_clean();
    $b64 = base64_encode($img);

    $data = array(

        'img' => $b64,
        'code' => $ticketcode

    );

    echo json_encode($data);
}
?>