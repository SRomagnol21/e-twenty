<?php

require_once 'bootstrap.php';

if(!isUserLoggedIn() || !isset($_POST["action"])){
    header("location: login-home.php");
}

//Inserimento
if($_POST["action"]==1){
    if(!allFieldsNewEvent()) {
        $templateParams["fielderror"] = "Inserire tutti i campi";
        header("location: manage-event.php");
    }
    $title = $_POST["title"];
    $eventDate = $_POST["eventDate"];
    $eventTime = $_POST["eventTime"];
    $about = $_POST["about"];
    $categoryCode = $_POST["categoryCode"];
    $placeCode = $dbh->getPlaceByName($_POST["placeName"])[0]["placeCode"];
    $username = $_SESSION["username"];

    list($result, $msg) = uploadImage($_SERVER["DOCUMENT_ROOT"].UPLOAD_DIR, $_FILES["eventPic"]);
    if($result != 0){
        $imgarticolo = $msg;
        $eventcode = $dbh->insertEvent($title, $eventDate, $eventTime, $about, $categoryCode, $placeCode, $_FILES["eventPic"]["name"], $username);
        if($eventcode!=false){
            $msg = "Inserimento completato correttamente!";
            $dbh->insertPriceSlot($eventcode, $_POST["slotCode1"], $_POST["position1"], $_POST["price1"], $_POST["limit1"]);
            $dbh->insertPriceSlot($eventcode, $_POST["slotCode2"], $_POST["position2"], $_POST["price2"], $_POST["limit2"]);
            $dbh->insertPriceSlot($eventcode, $_POST["slotCode3"], $_POST["position3"], $_POST["price3"], $_POST["limit3"]);
        } else{
            $msg = "Errore in inserimento!";
        }
    }
    header("location: login-home.php?formmsg=".$msg);
}

//Modifica
if($_POST["action"]==2){
    if(!allFieldsNewEvent()) {
        $templateParams["fielderror"] = "Inserire tutti i campi";
        header("location: manage-event.php");
    }
    $eventCode = $_POST["eventCode"];
    $title = $_POST["title"];
    $eventDate = $_POST["eventDate"];
    $eventTime = $_POST["eventTime"];
    $about = $_POST["about"];
    $categoryCode = $_POST["categoryCode"];
    $placeCode = $dbh->getPlaceByName($_POST["placeName"])[0]["placeCode"];
    
    if(isset($_FILES["eventPic"]) && strlen($_FILES["eventPic"]["name"])>0){
        list($result, $msg) = uploadImage($_SERVER["DOCUMENT_ROOT"].UPLOAD_DIR, $_FILES["eventPic"]);
        if($result == 0){
            header("location: login-home.php?formmsg=".$msg);
        }
        $imgarticolo = $msg;
    }
    else{
        $imgarticolo = $_POST["oldimg"];
    }
    
    $dbh->updateEvent($eventCode, $title, $eventDate, $eventTime, $about, $categoryCode, $placeCode, $_FILES["eventPic"]["name"]);
    $msg = "Modifica completata correttamente!";
    $dbh->updatePriceSlot($eventCode, $_POST["slotCode1"], $_POST["position1"], $_POST["price1"], $_POST["limit1"]);
    if($_POST["price2"]!="" && $_POST["position2"]!="") {
        $dbh->updatePriceSlot($eventCode, $_POST["slotCode2"], $_POST["position2"], $_POST["price2"], $_POST["limit2"]);
    }
    if($_POST["price3"]!="" && $_POST["position3"]!="") {
        $dbh->updatePriceSlot($eventCode, $_POST["slotCode3"], $_POST["position3"], $_POST["price3"], $_POST["limit3"]);
    }
    header("location: login-home.php?formmsg=".$msg);
}

//Cancellazione
if($_POST["action"]==3){
    $eventCode = $_POST["eventCode"];
    $dbh->deleteEvent($eventCode);
    
    $msg = "Cancellazione completata correttamente!";
    header("location: login-home.php?formmsg=".$msg);
}


?>