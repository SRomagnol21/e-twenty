<?php 
    $not = $dbh->getNotification($_POST["notificationcode"])[0];
?>
<div class="container">
    <div class="row mt-5">
        <div class="col-8 col-sm-6 offset-sm-2 text-left py-2">
            <p><?php echo $not["senderusername"]." ".$not["about"]; ?></p>
        </div>
        <div class="col-4 col-sm-2 text-right py-2">
            <form action="notification.php" method="POST">
                <input type="hidden" name="ok" value="ok">
                <label for="ok" class="d-none">OK</label><input id="ok" type="submit" class="btn btn-primary" value="OK"></input>
            </form>
        </div>
    </div>
</div>