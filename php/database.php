<?php
class DatabaseHelper
{
    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if ($this->db->connect_error) {
            die("Connection failed: " . $this->db->connect_error);
        }
    }

    //Private
    private function saltPassword($password, &$salt) {
        $salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
        return hash('sha512', $password.$salt);
    }

    //Public
    public function getConnection() {
        return $this->db;
    }

    //Users
    public function allAdmins() {
        $query = "SELECT username from admin";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getUserType($username) {
        $cquery = "SELECT username FROM customer WHERE username = ?";
        $cstmt = $this->db->prepare($cquery);
        $cstmt->bind_param("s", $username);
        $cstmt->execute();
        $cresult = $cstmt->get_result();
        if (mysqli_num_rows($cresult) == 1) {
            return "customer";
        } else {
            $oquery = "SELECT username FROM organizer WHERE username = ?";
            $ostmt = $this->db->prepare($oquery);
            $ostmt->bind_param('s', $username);
            $ostmt->execute();
            $oresult = $ostmt->get_result();
            if (mysqli_num_rows($oresult) == 1) {
                return "organizer";
            } else {
                $aquery = "SELECT username FROM admin WHERE username = ?";
                $astmt = $this->db->prepare($aquery);
                $astmt->bind_param('s', $username);
                $astmt->execute();
                $aresult = $astmt->get_result();
                if (mysqli_num_rows($aresult) == 1) {
                    return "admin";
                }
            }
        }
        return "no-user";
    }

    public function getOrganizerData($username){
        $query = "SELECT firstname, lastname, username, email, birthdate, country, region, province, city, active
            FROM organizer WHERE username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAdminData($username){
        $query = "SELECT firstname, lastname, username, email, birthdate, country, region, province, city
            FROM admin WHERE username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCustomerData($username){
        $query = "SELECT firstname, lastname, username, email, birthdate, country, region, province, city
            FROM customer WHERE username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCustomersOfEvent($eventCode){
        $stmt = $this->db->prepare("SELECT username FROM ticket WHERE eventcode = ? GROUP BY username");
        $stmt->bind_param('i',$eventCode);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllOrganizers(){
        $stmt = $this->db->prepare("SELECT username, active FROM organizer ORDER BY username DESC");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //Login
    public function checkLogin($username, $password){
        $type = $this->getUserType($username);
        if($type == "customer") {
            $cquery = "SELECT username, firstname, lastname, userpassword, salt FROM customer WHERE username = ?";
            $cstmt = $this->db->prepare($cquery);
            $cstmt->bind_param("s", $username);
            $cstmt->execute();
            $cstmt->store_result();
            $cstmt->bind_result($user, $firstname, $lastname, $hashPassword, $salt);
            $cstmt->fetch();
            $password = hash('sha512', $password.$salt);
            if($cstmt->num_rows == 1 && $hashPassword == $password) {
                $_SESSION["user"] = "customer";
                $_SESSION["username"] = $user;
                $_SESSION["firstname"] = $firstname;
                $_SESSION["lastname"] = $lastname;
                return true;
            }
        } else if($type == "organizer") {
                $oquery = "SELECT username, firstname, lastname, userpassword, salt FROM organizer WHERE username = ?";
                $ostmt = $this->db->prepare($oquery);
                $ostmt->bind_param("s", $username);
                $ostmt->execute();
                $ostmt->store_result();
                $ostmt->bind_result($user, $firstname, $lastname, $hashPassword, $salt);
                $ostmt->fetch();
                $password = hash('sha512', $password.$salt);
                if($ostmt->num_rows == 1 && $hashPassword == $password) {
                    $_SESSION["user"] = "organizer";
                    $_SESSION["username"] = $user;
                    $_SESSION["firstname"] = $firstname;
                    $_SESSION["lastname"] = $lastname;
                    return true;
                }
            } else if($type == "admin") {
                    $aquery = "SELECT username, firstname, lastname, userpassword, salt FROM admin WHERE username = ?";
                    $astmt = $this->db->prepare($aquery);
                    $astmt->bind_param('s', $username);
                    $astmt->execute();
                    $astmt->store_result();
                    $astmt->bind_result($user, $firstname, $lastname, $hashPassword, $salt);
                    $astmt->fetch();
                    $password = hash('sha512', $password.$salt);
                    if($astmt->num_rows == 1 && $hashPassword == $password) {
                        $_SESSION["user"] = "admin";
                        $_SESSION["username"] = $user;
                        $_SESSION["firstname"] = $firstname;
                        $_SESSION["lastname"] = $lastname;
                        return true;
                    }
                }

        return false;
    }

    //Registration
    public function registerCustomer($firstname, $lastname, $username, $email, $password, $city, $province, $region, $country, $birthdate) {
        $hashPassword = $this->saltPassword($password, $salt);
        $query = "INSERT INTO customer(firstname, lastname, username, email, userpassword, salt, city, province, region, country, birthdate)
                values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param(
            'sssssssssss',
            $firstname,
            $lastname,
            $username,
            $email,
            $hashPassword,
            $salt,
            $city,
            $province,
            $region,
            $country,
            $birthdate
        );
        return $stmt->execute();
    }

    public function registerOrganizer($firstname, $lastname, $username, $email, $password, $city, $province, $region, $country, $birthdate){
        $hashPassword = $this->saltPassword($password, $salt);
        $query = "INSERT INTO organizer(firstname, lastname, username, email, userpassword, salt, city, province, region, country, birthdate, active)
            values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param(
            'sssssssssss',
            $firstname,
            $lastname,
            $username,
            $email,
            $hashPassword,
            $salt,
            $city,
            $province,
            $region,
            $country,
            $birthdate
        );
        return $stmt->execute();
    }

    //Update
    public function updateProfile($username, $firstname, $lastname, $email, $city, $province, $region, $country, $birthdate, $oldusername){
        $type = $this->getUserType($oldusername);
        if($type == "customer"){
            $query = "UPDATE customer SET username = ?, firstname = ?, lastname = ?, email = ?, city = ?, province = ?, region = ?,
            country = ?, birthdate = ? WHERE username = ?";
        } else if($type == "organizer"){
            $query = "UPDATE organizer SET username = ?, firstname = ?, lastname = ?, email = ?, city = ?, province = ?, region = ?,
            country = ?, birthdate = ? WHERE username = ?";
        } else if($type == "admin"){
            $query = "UPDATE admin SET username = ?, firstname = ?, lastname = ?, email = ?, city = ?, province = ?, region = ?,
            country = ?, birthdate = ? WHERE username = ?";
        }
        
        $stmt = $this->db->prepare($query);
        $stmt->bind_param(
            'ssssssssss',
            $username,
            $firstname,
            $lastname,
            $email,
            $city,
            $province,
            $region,
            $country,
            $birthdate,
            $oldusername
        );
        return $stmt->execute();
    }

    private function checkpassword($password, $username){
        $type = $this->getUserType($username);
        if($type == "customer"){
            $query = "SELECT userpassword, salt FROM customer WHERE username = ?";
        } else if($type == "organizer"){
            $query = "SELECT userpassword, salt FROM organizer WHERE username = ?";
        } else if($type == "admin"){
            $query = "SELECT userpassword, salt FROM admin WHERE username = ?";
        }
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($userpassword, $salt);
        $stmt->fetch();
        $password = hash("sha512", $password.$salt);
        if($userpassword == $password){
            return true;
        }
        return false;
    }

    public function updatePassword($oldpassword, $newpassword, $username){
        $newHashPassword = $this->saltPassword($newpassword, $salt);
        $type = $this->getUserType($username);
        if($this->checkpassword($oldpassword, $username)){
            if($type == "customer"){
                $query = "UPDATE customer SET userpassword = ?, salt = ? WHERE username = ?";
            } else if($type == "organizer"){
                $query = "UPDATE organizer SET userpassword = ?, salt = ? WHERE username = ?";
            } else if($type == "admin"){
                $query = "UPDATE admin SET userpassword = ?, salt = ? WHERE username = ?";
            }
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('sss', $newHashPassword, $salt, $username);
            $stmt->execute();
            return true;
        }
        return false;
    }

    //Notifications
    public function getNotificationsByUsername($username){
        $query = "";
        if ($_SESSION["user"] == "customer") {
            $query = "SELECT shortdesc, about, senderusername FROM notification, notification_sample, customer WHERE username = ? AND username = destcustomer AND notificationkey = criptokey";
        } else if ($_SESSION["user"] == "organizer") {
            $query = "SELECT shortdesc, about, senderusername FROM notification, notification_sample, organizer WHERE username = ? AND username = destorganizer AND notificationkey = criptokey";
        } else if ($_SESSION["user"] == "admin") {
            $query = "SELECT shortdesc, about, senderusername FROM notification, notification_sample, admin WHERE username = ? AND username = destadmin AND notificationkey = criptokey";
        }
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getSenderByNotificationCode($notificationcode){
        $query = "SELECT senderusername FROM notification WHERE notificationcode = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $notificationcode);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotification($notificationcode) {
        $query = "SELECT shortdesc, about, notificationread, senderusername, eventCode FROM notification, notification_sample WHERE notificationcode= ? AND criptokey = notificationkey";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $notificationcode);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllMyNotifications($username, $usertype) {
        $query="";
        if($usertype=="customer") {
            $query = "SELECT notificationcode, senderusername, notificationdate, notificationtime, shortdesc, about, notificationread FROM notification, notification_sample WHERE destcustomer = ? AND criptokey = notificationkey ORDER BY notificationdate DESC, notificationtime DESC";
        } else if($usertype=="organizer") {
            $query = "SELECT notificationcode, senderusername, notificationdate, notificationtime, shortdesc, about, notificationread FROM notification, notification_sample WHERE destorganizer = ? AND criptokey = notificationkey ORDER BY notificationdate DESC, notificationtime DESC";
        } else if($usertype=="admin") {
            $query = "SELECT notificationcode, senderusername, notificationdate, notificationtime, shortdesc, about, notificationread FROM notification, notification_sample WHERE destadmin = ? AND criptokey = notificationkey ORDER BY notificationdate DESC, notificationtime DESC";
        }
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCriptoByCode($notificationcode) {
        $query = "SELECT criptokey FROM notification WHERE notificationcode= ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $notificationcode);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function removeNotificationFromAdmins($criptokey, $senderusername) {
        $admins = $this->allAdmins();
        foreach($admins as $admin) {
            $query = "DELETE FROM notification WHERE criptokey = ? AND senderusername = ? AND destadmin = ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('sss', $criptokey, $senderusername, $admin["username"]);
            $stmt->execute();
        }
    }

    public function removeNotificationFromMe($criptokey, $senderusername, $myusername) {
        $query = "DELETE FROM notification WHERE criptokey = ? AND senderusername = ? AND (destcustomer = ? OR destorganizer = ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssss', $criptokey, $senderusername, $myusername, $myusername);
        $stmt->execute();
    }

    public function getNotificationSender($notificationcode) {
        $query = "SELECT senderusername FROM notification WHERE notificationcode= ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $notificationcode);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function activeOrganizer($username) {
        $query = "UPDATE organizer SET active = 1 WHERE username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
    }

    public function deactiveOrganizer($username) {
        $query = "UPDATE organizer SET active = 0 WHERE username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
    }

    public function readNotification($notificationcode) {
        $query = "UPDATE notification SET notificationread = true WHERE notificationcode = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $notificationcode);
        return $stmt->execute();
    }

    public function getEventCodeByNotification($notificationcode) {
        $stmt = $this->db->prepare("SELECT eventCode FROM notification WHERE notificationCode = ?");
        $stmt->bind_param('i', $notificationcode);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //Events
	public function getCategories(){
        $stmt = $this->db->prepare("SELECT categoryCode, categoryName FROM category");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsByAuthor($author){
        $stmt = $this->db->prepare("SELECT eventCode, title, eventDate, eventTime, about, categoryCode, placeCode, eventPic FROM events WHERE username = ? AND deleted = 0 ORDER BY eventDate");
        $stmt->bind_param('s', $author);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventById($id){
        $stmt = $this->db->prepare("SELECT e.eventCode, e.title, e.eventDate, e.eventTime, e.about, e.categoryCode, e.placeCode, e.userName, e.eventPic, p.placeName, p.country, p.region, p.province, p.city, e.deleted FROM events e, place p WHERE p.placeCode = e.placeCode AND eventCode = ? AND e.deleted = 0");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventByIdAlsoDeleted($id){
        $stmt = $this->db->prepare("SELECT e.eventCode, e.title, e.eventDate, e.eventTime, e.about, e.categoryCode, e.placeCode, e.userName, e.eventPic, p.placeName, p.country, p.region, p.province, p.city, e.deleted FROM events e, place p WHERE p.placeCode = e.placeCode AND eventCode = ?");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }    

    public function insertEvent($title, $eventDate, $eventTime, $about, $categoryCode, $placeCode, $eventPic, $username){
        $stmt = $this->db->prepare("INSERT INTO events(title, eventDate, eventTime, about, categoryCode, placeCode, eventPic, username, deleted)
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, 0)");
        $stmt->bind_param('ssssiiss', $title, $eventDate, $eventTime, $about, $categoryCode, $placeCode, $eventPic, $username);
        $stmt->execute();
        return $stmt->insert_id;
    }

    public function updateEvent($eventcode, $title, $eventDate, $eventTime, $about, $categoryCode, $placeCode, $eventPic){
        $stmt = $this->db->prepare("UPDATE `events` SET `title` = ?, `eventDate` = ?, `eventTime` = ?, `about` = ?, `categoryCode` = ?, `placeCode` = ?, `eventPic` = ? WHERE eventcode = ?");
        $stmt->bind_param('ssssiisi', $title, $eventDate, $eventTime, $about, $categoryCode, $placeCode, $eventPic, $eventcode);
        $stmt->execute();
    }

    public function deleteEvent($eventCode){
        $stmt = $this->db->prepare("UPDATE events SET Deleted = 1 WHERE eventCode = ?");
        $stmt->bind_param('s', $eventCode);
        $stmt->execute();
    }

    public function getPopularEvents(){
        $today = date("Y-m-d");
        $stmt = $this->db->prepare("SELECT e.eventCode, e.title, e.eventDate, e.eventTime, e.about, e.categoryCode, e.placeCode, 
                                            e.eventPic, e.username, t.TicketCode, COUNT(e.eventCode) AS quantity
                                    FROM events AS e, ticket AS t 
                                    WHERE t.eventCode = e.eventCode 
                                    AND e.eventDate > ?
                                    AND deleted = 0 
                                    GROUP BY e.EventCode
                                    ORDER BY quantity DESC
                                    LIMIT 4");
        $stmt->bind_param('d', $today);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllPopularEvents(){
        $today = date("Y-m-d");
        $stmt = $this->db->prepare("SELECT e.eventCode, e.title, e.eventDate, e.eventTime, e.about, e.categoryCode, e.placeCode, 
                                            e.eventPic, e.username, t.TicketCode, COUNT(e.eventCode) AS quantity
                                    FROM events AS e, ticket AS t 
                                    WHERE t.eventCode = e.eventCode 
                                    AND e.eventDate > ?
                                    AND deleted = 0 
                                    GROUP BY e.EventCode
                                    ORDER BY quantity DESC");
        $stmt->bind_param('d', $today);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllEvents(){
        $today = date("Y-m-d");
        $stmt = $this->db->prepare("SELECT eventCode, title, eventDate, eventTime, about, categoryCode, placeCode, eventPic, username
                                    FROM events WHERE eventDate > ? AND deleted = 0 GROUP BY eventCode ORDER BY eventCode");
        $stmt->bind_param('d', $today);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPopularEventsByCategory($category){
        $today = date("Y-m-d");
        $stmt = $this->db->prepare("SELECT e.eventCode, e.title, e.eventDate, e.eventTime, e.about, e.categoryCode, e.placeCode, 
                                            e.eventPic, e.username, t.TicketCode, COUNT(e.eventCode) AS quantity
                                    FROM events AS e, ticket AS t, category AS c 
                                    WHERE t.eventCode = e.eventCode 
                                    AND e.categoryCode = c.categoryCode
                                    AND c.categoryCode = ?
                                    AND e.eventDate > ?
                                    AND deleted = 0 
                                    GROUP BY e.EventCode
                                    ORDER BY quantity DESC
                                    LIMIT 4");
        $stmt->bind_param('sd', $category, $today);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllEventsFromCategory($category){
        $today = date("Y-m-d");
        $stmt = $this->db->prepare("SELECT e.eventCode, e.title, e.eventDate, e.eventTime, e.about, e.categoryCode, e.placeCode, 
                                            e.eventPic, e.username
                                    FROM events AS e, category AS c 
                                    WHERE e.categoryCode = c.categoryCode
                                    AND c.categoryName = ?
                                    AND e.eventDate > ?
                                    AND deleted = 0");
        $stmt->bind_param('sd', $category, $today);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function searchEvents($string){
        $stmt = $this->db->prepare("SELECT eventCode, title, eventDate, eventTime, about, categoryCode, placeName, eventPic, username FROM events e, place p WHERE e.placeCode = p.placeCode AND (title LIKE ? OR username LIKE ? OR placeName LIKE ?) AND deleted = 0 ");
        $string = "%".$string."%";
        $stmt->bind_param('sss', $string, $string, $string);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getSoldTicketsForEventAndSlot($eventCode, $slotCode) {
        $stmt = $this->db->prepare("SELECT COUNT(e.eventCode) FROM events e, ticket t WHERE t.eventCode = e.eventCode AND t.eventCode = ? AND t.slotCode = ?");
        $stmt->bind_param('ii', $eventCode, $slotCode);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //Price Slots
    public function getPriceSlotsByEventId($eventcode) {
        $stmt = $this->db->prepare("SELECT slotCode, position, price, maxLimit FROM price_slot WHERE eventCode = ?");
        $stmt->bind_param('i', $eventcode);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertPriceSlot($eventcode, $slotcode, $position, $price, $maxlimit) {
        $stmt = $this->db->prepare("INSERT INTO price_slot(eventcode, slotcode, position, price, maxlimit) VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param('iisii', $eventcode, $slotcode, $position, $price, $maxlimit);
        $stmt->execute();
        return;
    }

    public function updatePriceSlot($eventcode, $slotcode, $position, $price, $maxLimit){
        $stmt = $this->db->prepare("UPDATE price_slot SET position = ?, price = ?, maxLimit = ? WHERE eventcode = ? AND slotcode = ?");
        $stmt->bind_param('siiii', $position, $price, $maxLimit, $eventcode, $slotcode);
        $stmt->execute();
        return $stmt->insert_id;
    }

    public function getSlotCodeByName($eventcode, $position) {
        $stmt = $this->db->prepare("SELECT slotCode, maxLimit FROM price_slot WHERE eventcode = ? AND position = ?");
        $stmt->bind_param('is', $eventcode, $position);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getSlotByCode($eventcode, $slotcode) {
        $stmt = $this->db->prepare("SELECT position, price, maxLimit FROM price_slot WHERE eventcode = ? AND slotcode = ?");
        $stmt->bind_param('ii', $eventcode, $slotcode);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    //Tickets
    public function insertTicket($eventcode, $slotcode, $username){
        $stmt = $this->db->prepare("INSERT INTO ticket(eventcode, slotcode, username) VALUES (?, ?, ?)");
        $stmt->bind_param('iis', $eventcode, $slotcode, $username);
        $stmt->execute();
        return $stmt->insert_id;
    }

    public function getLastnTickets($n, $username) {
        $stmt = $this->db->prepare("SELECT ticketcode, eventcode, slotcode, username FROM ticket WHERE username = ? ORDER BY ticketcode DESC LIMIT ?");
        $stmt->bind_param('si', $username, $n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMyTickets($username) {
        $stmt = $this->db->prepare("SELECT t.ticketcode, t.eventcode, t.slotcode, t.username FROM ticket AS t, events AS e WHERE t.username = ? AND e.deleted = 0 AND t.eventcode = e.eventcode");
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getTicket($ticketcode) {
        $stmt = $this->db->prepare("SELECT ticketcode, eventcode, slotcode, username FROM ticket WHERE ticketcode = ?");
        $stmt->bind_param('i', $ticketcode);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //Places
    public function addPlace($country, $region, $province, $city, $name) {
        $stmt = $this->db->prepare("INSERT INTO place(country, region, province, city, placeName) VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param('sssss', $country, $region, $province, $city, $name);
        $stmt->execute();
        return $stmt->insert_id;
    }

    public function getAllPlaces() {
        $stmt = $this->db->prepare("SELECT placeCode, placeName, city, province, region, country FROM place ORDER BY placeName");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPlaceByName($name) {
        $stmt = $this->db->prepare("SELECT placeCode, placeName, city, province, region, country FROM place WHERE placeName = ?");
        $stmt->bind_param('s', $name);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

}
