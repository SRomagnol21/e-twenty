<div class="container">
    <div class="row mt-5">
        <div class="col-sm-4 offset-sm-4 text-center">
            <p>Non hai eseguito l'accesso.</p>
            <p><a href="login.php">Accedi</a> o <a href="register.php">registrati</a> per poter effettuare acquisti.</p>
        </div>
    </div>
</div>