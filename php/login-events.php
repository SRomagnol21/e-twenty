<div class="container">
    <div class="row">
        <div class="col-12" id="organizerTitle">
            <?php if(isOrganizerLoggedIn()):?>
            <h2>Eventi di <?php echo $_SESSION["firstname"]." ".$_SESSION["lastname"]; ?></h2>
            <?php elseif(isAdminLoggedIn()):?>
            <h2>Lista Eventi</h2>
            <?php endif;?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 align-self-center offset-sm-3">
            <p><?php echo $templateParams["formmsg"];?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 align-self-center offset-sm-3" id="newEventCol">
            <?php if(isOrganizerLoggedIn() && $dbh->getOrganizerData($_SESSION["username"])[0]["active"]):?>
                <a href="manage-event.php?action=1" class="btn btn-primary" id="newEvent">Inserisci nuovo evento</a>
            <?php elseif(!isAdminLoggedIn()): ?>
                <p>Non puoi inserire eventi perchè sei ancora in attesa di essere attivato.</p>
            <?php endif; ?>
        </div>
    </div>
    <?php if(isAdminLoggedIn()): $templateParams["eventi"] = $dbh->getAllEvents(); endif; ?>
    <?php foreach($templateParams["eventi"] as $event): ?>
        <div class="row rounded item">
            <div class="col-xs-10 align-self-center" id="organizerEventTitle">
                <p><?php echo $event["title"] ?></p>
            </div>
            <div class="col-1 align-self-left" id="organizerButton">
                <a href="manage-event.php?action=2&id=<?php echo $event["eventCode"]; ?>" id="modifica"><i class="fa fa-edit"></i></a>
            </div>
            <div class="col-1 align-self-right" id="organizerButton">
                <a href="manage-event.php?action=3&id=<?php echo $event["eventCode"]; ?>" id="elimina"><i class="fa fa-trash"></i></a>
            </div>
        </div>
    <?php endforeach; ?>
</div>