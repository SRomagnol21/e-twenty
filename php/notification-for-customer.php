<?php 
    $not = $dbh->getNotification($_POST["notificationcode"])[0];
    $event = $dbh->getEventByIdAlsoDeleted($not["eventCode"]);
?>
<div class="container">
    <div class="row">
        <div class="col-12 title">
            <h2>I miei biglietti</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-center">
            <p>L'evento <?php echo $event[0]["title"];?> <?php echo $not["about"]; ?></p>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-6 offset-sm-3 text-right">
            <form action="event.php" method="POST">
                <input type="hidden" name="eventcode" value="<?php echo $not["eventCode"]; ?>">
                <label for="toevent" class="d-none">Vedi evento</label><input id="toevent" type="submit" class="btn btn-primary" value="Vai all'evento"></input>
            </form>
        </div>
    </div>
</div>