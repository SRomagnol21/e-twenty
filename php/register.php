<?php 
require_once 'bootstrap.php';

$_SESSION["page"]="register";

if(isset($_POST["firstname"]) && isset($_POST["lastname"]) && isset($_POST["birthdate"]) && isset($_POST["country"]) && isset($_POST["region"]) &&
isset($_POST["city"]) && isset($_POST["province"]) && isset($_POST["username"]) && isset($_POST["email"]) && isset($_POST["password"]) && 
isset($_POST["password2"]) && isset($_POST["userType"]) && $_POST["password"]==$_POST["password2"]) {
    
        if(allFieldsRegistration()) {
            if($_POST["userType"] == "customer") {
                
                $dbh->registerCustomer($_POST["firstname"], $_POST["lastname"], $_POST["username"], $_POST["email"], $_POST["password"], $_POST["city"], $_POST["province"], $_POST["region"], 
                    $_POST["country"], $_POST["birthdate"]);
                
                $login = $dbh->checkLogin($_POST["username"], $_POST["password"]);
                if(!$login){
                    $templateParams["errorelogin"] = "Errore! Controllare username o password!";
                }

            } else {
                
                $_SESSION["dest"]="admin";
                $_SESSION["notifica"]="subscription";

                $dbh->registerOrganizer($_POST["firstname"], $_POST["lastname"], $_POST["username"], $_POST["email"], $_POST["password"], $_POST["city"], $_POST["province"], $_POST["region"], 
                    $_POST["country"], $_POST["birthdate"]);
                
                $login = $dbh->checkLogin($_POST["username"], $_POST["password"]);
                if(!$login){
                    $templateParams["errorelogin"] = "Errore! Controllare username o password!";
                }
            }
        }
        else {
            $templateParams["erroreregistrazione"] = "Inserire tutti i campi";
        }
} else {
    $templateParams["erroreregistrazione"] = "Inserire tutti i campi";
}
if(isset($_POST["password"]) && isset($_POST["password2"]) && $_POST["password"]!=$_POST["password2"]) {
    $templateParams["errorelogin"] = "Errore! Controllare username o password!";
}
if(isUserLoggedIn()) {
    $templateParams["titolo"] = " e20 - Registrazione Effettuata";
    $templateParams["nome"] = "register-ok.php";
}
else {    
    $templateParams["titolo"] = " e20 - Registrazione Utente";
    $templateParams["nome"] = "register-user.php";
}

require 'template/base.php';
?>