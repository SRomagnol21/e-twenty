<?php
if($dbh->getUserType($_SESSION["username"]) == "customer"){
    $user = $dbh->getCustomerData($_SESSION["username"]);
} elseif($dbh->getUserType($_SESSION["username"]) == "organizer"){
    $user = $dbh->getOrganizerData($_SESSION["username"]);
} elseif($dbh->getUserType($_SESSION["username"]) == "admin"){
    $user = $dbh->getAdminData($_SESSION["username"]);
}
?>
<div class="container-fluid">
        <div class="row">
            <div class="col-sm-2 align-self-center offset-sm-5">
                <h1 id="loginHeader">Modifica dati</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 align-self-center offset-sm-4">
                <form action="login.php" method="POST" id="save-edits">
                    <p>Dati personali</p>
                    <label class="d-none" for="firstname">Nome</label><input class="rounded" type="text" id="firstname" name="firstname" value="<?php echo $user[0]["firstname"]?>"/>
                    <label class="d-none" for="lastname">Cognome</label><input class="rounded" type="text" id="lastname" name="lastname" value="<?php echo $user[0]["lastname"]?>"/>
                    <label class="d-none" for="birthdate">Data di nascita</label><input class="rounded" type="date" id="birthdate" name="birthdate" value="<?php echo $user[0]["birthdate"]?>"/>
                    <label class="d-none" for="country">Paese</label><input class="rounded" type="text" id="country" name="country" value="<?php echo $user[0]["country"]?>"/>
                    <label class="d-none" for="region">Regione</label><input class="rounded" type="text" id="region" name="region" value="<?php echo $user[0]["region"]?>"/>
                    <label class="d-none" for="province">Provincia</label><input class="rounded" type="text" id="province" name="province" value="<?php echo $user[0]["province"]?>"/>
                    <label class="d-none" for="city">Citt&agrave;</label><input class="rounded" type="text" id="city" name="city" value="<?php echo $user[0]["city"]?>"/>
                    <hr>
                    <p>Dati di accesso</p>
                    <label class="d-none" for="username">Username</label><input class="rounded" type="text" id="username" name="username" value="<?php echo $user[0]["username"]?>" />
                    <label class="d-none" for="email">Email</label><input class="rounded" type="text" id="email" name="email" value="<?php echo $user[0]["email"]?>" />
                    <label class="d-none" for="submit">Salva</label><input class="btn btn-primary" id="submit" type="submit" name="save-edits" value="Salva">
                </form>
            </div>
        </div>
    </div>