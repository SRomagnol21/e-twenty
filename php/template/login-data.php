<?php
if($dbh->getUserType($_SESSION["username"]) == "customer"){
    $user = $dbh->getCustomerData($_SESSION["username"]);
} elseif($dbh->getUserType($_SESSION["username"]) == "organizer"){
    $user = $dbh->getOrganizerData($_SESSION["username"]);
} elseif($dbh->getUserType($_SESSION["username"]) == "admin"){
    $user = $dbh->getAdminData($_SESSION["username"]);
}
?>
<div class="container">
    <div class="row title">
        <div class="col-sm-4 offset-sm-4">
            <h2>Dati utente</h2>
        </div>
    </div>
    <form action="login.php" method="POST">
        <div class="row info">
            <div class="col-6">
                <p>Username:</p>
            </div>
            <div class="col-6">
                <p><?php echo $user[0]["username"]; ?></p>
            </div>
        </div>
        <div class="row info">
            <div class="col-6">
                <p>Nome:</p>
            </div>
            <div class="col-6">
                <p><?php echo $user[0]["firstname"]; ?></p>
            </div>
        </div>
        <div class="row info">
            <div class="col-6">
                <p>Cognome:</p>
            </div>
            <div class="col-6">
                <p><?php echo $user[0]["lastname"]; ?></p>
            </div>
        </div>
        <div class="row info">
            <div class="col-6">
                <p>Data di nascita:</p>
            </div>
            <div class="col-6">
                <p><?php echo $user[0]["birthdate"]; ?></p>
            </div>
        </div>
        <div class="row info">
            <div class="col-6">
                <p>Paese:</p>
            </div>
            <div class="col-6">
                <p><?php echo $user[0]["country"]; ?></p>
            </div>
        </div>
        <div class="row info">
            <div class="col-6">
                <p>Regione:</p>
            </div>
            <div class="col-6">
                <p><?php echo $user[0]["region"]; ?></p>
            </div>
        </div>
        <div class="row info">
            <div class="col-6">
                <p>Provincia:</p>
            </div>
            <div class="col-6">
                <p><?php echo $user[0]["province"]; ?></p>
            </div>
        </div>
        <div class="row info">
            <div class="col-6">
                <p>Citt&agrave;:</p>
            </div>
            <div class="col-6">
                <p><?php echo $user[0]["city"]; ?></p>
            </div>
        </div>
        <div class="row mt-3 text-right">
            <div class="col-12">
                <label class="d-none" for="update-profile">Modifica dati</label><input type="submit" name="update-profile" id="update-profile" class="btn btn-primary mr-2" value="Modifica dati"></input>
                <label class="d-none" for="update-password">Modifica password</label><input type="submit" name="update-password" id="update-password" class="btn btn-primary" value="Modifica password"></input>
            </div>
        </div>
    </form>
</div>