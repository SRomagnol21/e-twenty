<?php
    $event = $templateParams["event"];
    $slots = $templateParams["slots"];
    $action = getAction($templateParams["action"]);
    $places = $dbh->getAllPlaces();
?>
<div class="container">
    <div class="row">
        <div class="col-12 title">
            <h2><?php echo $action; ?> Evento</h2>
        </div>
    </div>
    <form id="organizer-form" action="process-event.php" method="POST" enctype="multipart/form-data">
        <?php if($event == null): ?>
            <p>Evento non trovato</p>
        <?php else: ?>
            <div class="row">
                <div class="col-sm-4 align-self-center offset-sm-4">
                    <label class="d-none" for="title">Titolo</label><input class="rounded" type="text" id="title" name="title" value="<?php echo $event["title"]; ?>" placeholder="Titolo"/>
                    <label class="d-none" for="eventDate">Data</label><input class="rounded" type="date" id="eventDate" name="eventDate" value="<?php echo $event["eventDate"]; ?>"/>
                    <label class="d-none" for="eventTime">Ora</label><input class="rounded" type="time" id="eventTime" name="eventTime" value="<?php echo $event["eventTime"]; ?>"/>
                    <label class="d-none" for="about">Descrizione</label><textarea class="rounded" id="about" name="about" placeholder="Descrizione"><?php echo $event["about"]; ?></textarea>
                    <hr>
                    <p>Inserisci immagine</p>
                        <?php if($templateParams["action"]!=3): ?>
                            <label class="d-none" for="eventPic">Immagine</label><input class="rounded" type="file" name="eventPic" id="eventPic" />
                        <?php endif; ?>
                    <hr>
                    <p>Inserisci categoria</p>
                        <?php foreach($templateParams["categories"] as $categoria): ?>
                            <label class="d-none" for="<?php echo $categoria["categoryCode"]; ?>"><?php echo $categoria["categoryName"]; ?></label>
                            <input type="radio" for="categoryCode" id="<?php echo $categoria["categoryCode"]; ?>" name="categoryCode" 
                                <?php if($categoria["categoryCode"] == $event["categoryCode"]){ echo ' checked="checked" ';} ?> 
                                value="<?php echo $categoria["categoryCode"]; ?>"/> <label for="<?php echo $categoria["categoryCode"]; ?>">
                                <?php echo $categoria["categoryName"] ?></label></br>
                        <?php endforeach; ?>
                    <hr>
                    <p>Scegli il luogo</p>
                    <input type="hidden" id="placeSpaceInput" name="placeName" value="<?php echo $event["placeName"]; ?>"/>
                    <div class="dropdown">
                        <label class="d-none" for="placeSpaceBtn">Scegli luogo</label>
                        <button class="dropdown-toggle btn btn-outline-primary" type="button" data-toggle="dropdown" id="placeSpaceBtn" style="width: 100%;"><?php echo $event["placeName"]; ?>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" id="allPlaces" style="width: 100%;">
                            <?php foreach($places as $place):?>
                                <li class="place"><?php echo $place["placeName"];?></li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                    <p class="mt-2 text-center">Nel caso in cui non ci sia la localit&agrave; che cerchi<br>
                    <a class="text-primary" id="showNewPlace">aggiungi una nuova localit&agrave;</a>.</p>
                    <div id="no-style" class="hidePlaces not-showing w-100">
                        <p>Nuova Localit&agrave;</p>
                        <label class="d-none" for="newCountry">Paese</label><input class="rounded w-100" type="text" id="newCountry" name="newCountry" placeholder="Stato"/>
                        <label class="d-none" for="newRegion">Regione</label><input class="rounded w-100" type="text" id="newRegion" name="newRegion" placeholder="Regione"/>
                        <label class="d-none" for="newProvince">Provincia</label><input class="rounded w-100" type="text" id="newProvince" name="newProvince" placeholder="Provincia (sigla)"/>
                        <label class="d-none" for="newCity">Citt&agrave;</label><input class="rounded w-100" type="text" id="newCity" name="newCity" placeholder="Citt&agrave;"/>
                        <label class="d-none" for="newPlaceName">Nome luogo</label><input class="rounded w-100" type="text" id="newPlaceName" name="newPlaceName" placeholder="Nome luogo"/>
                        <label class="d-none" for="hideNewPlace">Annulla</label><button class="btn btn-outline-primary mb-3" id="hideNewPlace" type="button">Annulla</button>
                        <label class="d-none" for="addPlace">Aggiungi</label><button class="btn btn-primary mb-3" id="addPlace" type ="button">Aggiungi</button>
                    </div>
                    <hr>
                </div>
            <div class="col-sm-4 align-self-center offset-sm-4">
                <p>Inserisci fino a 3 fasce di prezzo</p>
                <label class="d-none" for="position1">Posizione</label><input class="rounded w-100" type="text" id="position1" name="position1" value="<?php echo $slots[0]["position"]; ?>" placeholder="Tipo biglietto"/>
                <label class="d-none" for="price1">Prezzo</label><input class="rounded w-100" type="text" id="price1" name="price1" value="<?php echo $slots[0]["price"]; ?>" placeholder="Prezzo"/>
                <label class="d-none" for="limit1">Limite</label><input class="rounded w-100" type="text" id="limit1" name="limit1" value="<?php echo $slots[0]["maxLimit"]; ?>" placeholder="Numero posti"/>
                <input type="hidden" id="slotCode1" name="slotCode1" value="<?php echo $slots[0]["slotCode"]; ?>">
                <div id="li2" class="not-showing">
                    <label class="d-none" for="position2">Posizione</label><input class="rounded w-100" type="text" id="position2" name="position2" value="<?php echo $slots[1]["position"]; ?>" placeholder="Tipo biglietto"/>
                    <label class="d-none" for="price2">Prezzo</label><input class="rounded w-100" type="text" id="price2" name="price2" value="<?php echo $slots[1]["price"]; ?>" placeholder="Prezzo"/>
                    <label class="d-none" for="limit2">Limite</label><input class="rounded w-100" type="text" id="limit2" name="limit2" value="<?php echo $slots[1]["maxLimit"]; ?>" placeholder="Numero posti"/>
                    <input type="hidden" id="slotCode2" name="slotCode2" value="<?php echo $slots[1]["slotCode"]; ?>">
                </div>
                <div id="li3" class="not-showing">
                    <label class="d-none" for="position3">Posizione</label><input class="rounded w-100" type="text" id="position3" name="position3" value="<?php echo $slots[2]["position"]; ?>" placeholder="Tipo biglietto"/>
                    <label class="d-none" for="price3">Prezzo</label><input class="rounded w-100" type="text" id="price3" name="price3" value="<?php echo $slots[2]["price"]; ?>" placeholder="Prezzo"/>
                    <label class="d-none" for="limit3">Limite</label><input class="rounded w-100" type="text" id="limit3" name="limit3" value="<?php echo $slots[2]["maxLimit"]; ?>" placeholder="Numero posti"/>
                    <input type="hidden" id="slotCode3" name="slotCode3" value="<?php echo $slots[2]["slotCode"]; ?>">
                </div>
            </div>
            <div class="col-sm-4 align-self-center offset-sm-4">
                <a class="btn btn-outline-primary" id="fa-minus">Rimuovi Fascia</a>
                <a class="btn btn-primary text-white" id="fa-plus">Aggiungi Fascia</a>
            </div>
            <div class="col-sm-4 align-self-center offset-sm-4">
                <?php if(isset($templateParams["fielderror"])):?>
                    <p class="text-danger"><?php echo $templateParams["fielderror"];?></p>
                <?php endif;?>
            </div>
            <?php if($templateParams["action"] != 1): ?>
                <input type="hidden" name="eventCode" value="<?php echo $event["eventCode"]; ?>">
                <input type="hidden" name="categoryCode" value="<?php echo $event["categoryCode"]; ?>">
            <?php endif; ?>
            <input type="hidden" name="action" value="<?php echo $templateParams["action"]; ?>">
            <div class="col-sm-4 align-self-center offset-sm-4 mt-4">
                <a id="organizer-form-annulla" href="login.php" class="float-left btn btn-outline-primary">Annulla</a>
                <label class="d-none" for="submit"><?php echo $action; ?> Evento</label><input type="submit" name="submit" class="float-right btn btn-primary w-50" <?php if($action=="Modifica"): echo 'id="modify"'; elseif($action=="Cancella"): echo 'id="delete"'; endif; ?>  value="<?php echo $action; ?> Evento"/>
            </div>
        </div>
        <?php endif; ?>
    </form>
</div>
