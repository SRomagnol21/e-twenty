<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2 align-self-center offset-sm-5">
            <img src="../img/logo2.png" alt="" id="loginLogo">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 align-self-center offset-sm-5">
            <h1 id="loginHeader">Registrati</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 align-self-center offset-sm-4">
            <h2 id="loginSubheader">Registrati per ottenere tutti i vantaggi di <b>e20</b>!</br>
                Possibilit&agrave; di acquistare biglietti, ricezione di notifiche riguardanti i tuoi eventi e tanto
                altro ancora.</br>
                Hai gi&agrave; un account? <a href="login.php">accedi ora</a>.</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 align-self-center offset-sm-4">
            <?php if (isset($templateParams["erroreregistrazione"])) : ?>
                <p class="text-danger"><?php echo $templateParams["erroreregistrazione"]; ?></p>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 align-self-center offset-sm-4">
            <form action="register.php" method="POST" id="registerForm">
                <p>Dati personali</p>
                <label for="firstname" class="d-none">Nome</label><input class="rounded" type="text" id="firstname" name="firstname" placeholder="nome" />
                <label for="lastname" class="d-none">Cognome</label><input class="rounded" type="text" id="lastname" name="lastname" placeholder="cognome" />
                <label for="birthdate" class="d-none">Data di nascita</label><input class="rounded" type="date" id="birthdate" name="birthdate" />
                <label for="country" class="d-none">Paese</label><input class="rounded" type="text" id="country" name="country" placeholder="paese" />
                <label for="region" class="d-none">Regione</label><input class="rounded" type="text" id="region" name="region" placeholder="regione" />
                <label for="province" class="d-none">Provincia</label><input class="rounded" type="text" id="province" name="province" placeholder="provincia" />
                <label for="city" class="d-none">Citt&agrave;</label><input class="rounded" type="text" id="city" name="city" placeholder="citt&agrave;" />
                <hr>
                <p>Dati di accesso</p>
                <div class="rounded" id="radioInput">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="userType" id="radio1" value="customer" checked>
                        <label class="form-check-label" for="radio1">
                            Utente
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="userType" id="radio2" value="organizer">
                        <label class="form-check-label" for="radio2">
                            Organizzatore
                        </label>
                    </div>
                </div id="radioInput">
                <label for="username" class="d-none">Username</label><input class="rounded" type="text" id="username" name="username" placeholder="username" />
                <label for="email" class="d-none">Email</label><input class="rounded" type="text" id="email" name="email" placeholder="email" />
                <p>Inserire almeno 8 caratteri, di cui almeno un numero</p>
                <label for="password" class="d-none">Password</label><input class="rounded" type="password" id="password" name="password" placeholder="password" /></br>
                <label for="password2" class="d-none">Conferma password</label><input class="rounded" type="password" id="password2" name="password2" placeholder="conferma password" />
                <label for="submit" class="d-none">Registrati</label><input class="btn btn-primary" id="submit" type="submit" name="submit" value="Registrati" disabled>
            </form>
        </div>
    </div>
</div>
<script>
    document.getElementById("submit").style.background = "grey";
    var psw1 = document.getElementById("password");
    var numbers = /[0-9]/g;
    psw1.onkeyup = function(){
        if(psw1.value.match(numbers) && psw1.value.length>=8) {
            document.getElementById("submit").style.background = "#b30000";
            document.getElementById("submit").disabled = false;
        } else {
            document.getElementById("submit").style.background = "grey";
            document.getElementById("submit").disabled = true;
        }
    };
</script>