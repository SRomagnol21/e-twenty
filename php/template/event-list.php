<form action="event.php" method="POST">
<div class="container">
    <div class="row">
        <div class="col-12 title">
            <?php if($_SESSION["page"]=="search"):?>
                <h2>Risultati ricerca per: <?php echo $_POST["search"]; ?></h2>
            <?php else: ?>
                <h2><?php echo $_SESSION["page"]; ?></h2>
            <?php endif; ?>
        </div>
    </div>
    <?php foreach($templateParams["events"] as $event): ?>
        <div class="card shadow my-4" id="<?php echo $event["eventCode"];?>">
            <h5 class="card-header align-middle py-3">
                <?php echo $event["title"] ?>
            </h5>
            <div class="card-body p-3">
                <h5 class="card-title"><?php echo $event["username"] ?></h5>
                <div class="row justify-content-end">
                    <div class="col-5">
                    <p class="card-text"><?php echo $event["eventDate"] ?></br><?php echo $event["eventTime"] ?></p>
                    </div>
                    <div class="col-7 text-right align-middle py-2">
                        <form action="event.php" method="POST">
                            <input type='hidden' name='eventcode' value="<?php echo $event["eventCode"];?>">
                            <label for="<?php echo $event["eventCode"] ?>" class="d-none">Dettagli</label><input type="submit" name="submit" id="<?php echo $event["eventCode"] ?>" value="Dettagli" class="btn btn-primary notification-button  px-3 py-2">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
</form>