<!DOCTYPE html>
<html lang="it">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="<?php echo STYLE . "?version=1.0" ?>" rel="stylesheet">
    <title>e20 - Home</title>
</head>

<body class="bg-light" id="ciao">
    <nav class="navbar navbar-expand-lg navbar-dark sticky-top">

        <!-- --------------LOGO/HOME-------------- -->
        <div class="navbar-nav p-0 m-0 mr-auto">
            <a class="navbar-brand nav-link rounded-lg" href="index.php"><img style="height: 35px; width: 100;" src="../img/logo2.png" alt="Home"></a>
        </div>

        <?php if (isset($_SESSION["username"])) : ?>

            <!-- --------------NOTIFICATION BUTTON-------------- -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <a class="nav-link" href="notification.php" role="button">
                    <span class="badge count" id="bell-count-2" style="border-radius:10px; background-color:#f5aa94; color: black;"></span>
                    <span class="fa fa-bell"></span>
                </a>
                <form action="notification.php" method="POST">
                    <label class="d-none" for="dropdown-menu-id-1">Notifications</label><ul class="dropdown-menu dropdown-menu-right dropdown-menu-notif" aria-labelledby="navbarDropdown" id="dropdown-menu-id-1"></ul>
                </form>
            </button>

            <!-- --------------CART BUTTON-------------- -->
            <?php if ($dbh->getUserType($_SESSION["username"]) == "customer") : ?>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <a class="nav-link" href="cart.php" role="button">
                    <span class="badge count" id="cart-count-2" style="border-radius:10px; background-color:#f5aa94; color: black;"></span>
                    <span class="fa fa-shopping-cart"></span>
                </a>
            </button>
            <?php endif; ?>
        <?php endif; ?>

        <!-- --------------SEARCH BUTTON-------------- -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedSearch" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-search"></i>
        </button>

        <!-- --------------HAMBURGER BUTTON-------------- -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class=" fa fa-bars"></span>
        </button>


        <div class="collapse navbar-collapse" id="navbarSupportedSearch">
            <span class="fa fa-search"></span>
            <form class="form-inline my-2 my-lg-0 ml-2" id="search" action="search.php" method="POST" for="search">
                <label class="d-none" for="search">Cerca</label><input class="form-control mr-sm-2" type="text" id="search" placeholder="cerca" aria-label="Search" name="search">
            </form>
        </div>

        <?php if (isset($_SESSION["username"])) : ?>
            <div class="collapse ml-auto navbar-collapse mr-1" id="navbarSupportedNotification">
                <ul class="navbar-nav ml-auto d-none d-lg-block">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="badge count" id="bell-count" style="border-radius:10px; background-color:#f5aa94; color: black;"></span>
                            <span class="fa fa-bell"></span>
                        </a>
                        <form action="notification.php" method="POST">
                            <label class="d-none" for="dropdown-menu-id">Notifications</label><ul class="dropdown-menu dropdown-menu-right dropdown-menu-notif" aria-labelledby="navbarDropdown" id="dropdown-menu-id"></ul>
                        </form>
                    </li>
                </ul>
            </div>
            <?php if ($dbh->getUserType($_SESSION["username"]) == "customer") : ?>
            <div class="collapse ml-auto navbar-collapse mr-4" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto d-none d-lg-block">
                    <li class="nav-item">
                        <a class="nav-link cart-icon" href="cart.php" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="badge count" id="cart-count" style="border-radius:10px; background-color:#f5aa94; color: black;"></span>
                            <span class="fa fa-shopping-cart"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <?php endif; ?>
        <?php endif; ?>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto d-none d-lg-block">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="fa fa-user" style="border-radius:10px;"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <?php if (!isset($_SESSION["username"])) : ?>
                            <a class="dropdown-item" href="login.php">Accedi</a>
                            <a class="dropdown-item" href="register.php">Registrati</a>
                        <?php else : ?>
                            <?php if (isAdminLoggedIn()) :?>
                                <a class="dropdown-item" href="login-organizerlist.php">Gestisci organizzatori</a>
                            <?php endif; ?>
                            <?php if (isOrganizerLoggedIn() || isAdminLoggedIn()) : ?>
                                <a class="dropdown-item" href="login-home.php">Gestisci eventi</a>
                            <?php endif; ?>
                            <?php if ($dbh->getUserType($_SESSION["username"]) == "customer") : ?>
                                <a class="dropdown-item" href="tickets.php">I miei biglietti</a>
                            <?php endif; ?>
                            <a class="dropdown-item" href="login.php">Il Mio Account</a>
                            <a class="dropdown-item" href="logout.php">Logout</a>
                        <?php endif; ?>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto d-lg-none">
                <?php if (!isset($_SESSION["username"])) : ?>
                    <li>
                        <a href="login.php">Accedi</a>
                    </li>
                    <li>
                        <a href="register.php">Registrati</a>
                    </li>
                <?php else : ?>
                    <?php if (isAdminLoggedIn()) :?>
                        <li>
                            <a href="login-organizerlist.php">Gestisci organizzatori</a>
                        </li>
                    <?php endif; ?>
                    <?php if (isOrganizerLoggedIn() || isAdminLoggedIn()) : ?>
                        <li>
                            <a href="login-home.php">Gestisci eventi</a>
                        </li>
                    <?php endif; ?>
                    <?php if ($dbh->getUserType($_SESSION["username"]) == "customer") : ?>
                        <li>
                            <a href="tickets.php">I miei biglietti</a>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a href="login.php">Il Mio Account</a>
                    </li>
                    <li>
                        <a href="logout.php">Logout</a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </nav>

    <main>
        <?php
        if (isset($templateParams["nome"])) {
            require($templateParams["nome"]);
        }
        ?>
    </main>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/script.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
<footer class="footer mt-5">
    <p>Copyright &copy; 2020 e20. Tutti i diritti riservati.</p>
</footer>

</html>