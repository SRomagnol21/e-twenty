<div class="container-fluid">
    <div class="row title">
        <div class="col-12">
            <h2>Modifica password</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 align-self-center offset-sm-4">
            <?php if(isset($templateParams["errorepassword"])): ?>
                <p class="text-danger"><?php echo $templateParams["errorepassword"]; ?></p>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 align-self-center offset-sm-4 text-right">
            <form action="login.php" method="POST" id="change-password">
                <label class="d-none" for="oldpassword">Vecchia password</label><input class="rounded" type="password" id="oldpassword" name="oldpassword" placeholder="Insert the old password"/>
                <label class="d-none" for="newpassword">Nuova password</label><input class="rounded" type="password" id="newpassword" name="newpassword" placeholder="Insert the new password"/>
                <label class="d-none" for="confirmpassword">Conferma password</label><input class="rounded" type="password" id="confirmpassword" name="confirmpassword" placeholder="Confirm the new password"/></br>
                <label class="d-none" for="leave-password">Annulla</label><input class="btn btn-outline-primary" id="leave-password" type="submit" name="leave-password" value="Annulla">
                <label class="d-none" for="save-password">Salva password</label><input class="btn btn-primary" id="save-password" type="submit" name="save-password" value="Salva password" disabled>
            </form>
        </div>
    </div>
</div>
<script>
    document.getElementById("save-password").style.background = "grey";
    var psw1 = document.getElementById("newpassword");
    var numbers = /[0-9]/g;
    psw1.onkeyup = function(){
        if(psw1.value.match(numbers) && psw1.value.length>=8) {
            document.getElementById("save-password").style.background = "#b30000";
            document.getElementById("save-password").disabled = false;
        } else {
            document.getElementById("save-password").style.background = "grey";
            document.getElementById("save-password").disabled = true;
        }
    };
</script>