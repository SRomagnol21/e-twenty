<?php 
    $popular=$dbh->getPopularEvents();
?>
<div class="container-fluid p-0 m-0">
    <div class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active" style="background-image: url(../img/eventi2.jpg);">
            </div>
            <div id="carousel-title">
                <h1>Fatto per chi fa</h1>
                <p>Scopri subito tantissimi eventi intorno a te.</p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
        <div class="card row" id="cardCollapse">
            <div class="card-header" id="heading0">
                <h2 class="mb-0">
                    <button class="btn btn-link" id="collapseButton" type="button" data-toggle="collapse"
                        data-target="#collapse0" aria-expanded="true" aria-controls="collapse0">
                        Eventi principali
                        <a href="main-events.php">vedi tutti</a>
                    </button>
                </h2>
            </div>
            
            <div id="collapse0" class="collapse show" aria-labelledby="heading0">
                <div class="container-fluid">
                    <div class="row" id="rowCollapse">
                        <div class="col-xl-2">
                        </div>
                        
                        <?php foreach($popular as $event):?>
                        <div class="col-md-6 col-lg-3 col-xl-2 event-card">
                            <div class="card event shadow">
                                <div class="card-image">
                                    <img src="<?php echo UPLOAD_DIR.$event["eventPic"];?>"  class="card-img-top img-fluid" alt="">
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $event["title"];?></h5>
                                    <div class="row my-3">
                                    <div class="col-12">
                                            <p class="card-text"><?php echo $event["about"];?></p>
                                        </div>
                                    </div>
                                    <div class="row my-3">
                                        <div class="col-12">
                                            <p class="card-text"><?php echo $event["eventDate"];?></p>
                                        </div>
                                    </div>
                                    
                                    <form action="event.php" method="POST">
                                        <input type="hidden" name="eventcode" value="<?php echo $event["eventCode"];?>">
                                        <label class="d-none" for="submit-dettagli">Dettagli</label><input type="submit" id="submit-dettagli" class="btn btn-primary" value="Dettagli">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>



        <?php foreach($dbh->getCategories() as $category): ?>

        <div class="card row" id="cardCollapse">
            <div class="card-header" id="heading<?php echo $category["categoryCode"]; ?>">
                <h2 class="mb-0">
                    <button class="btn btn-link" id="collapseButton" type="button" data-toggle="collapse"
                        data-target="#collapse<?php echo $category["categoryCode"]; ?>" aria-expanded="true" aria-controls="collapse<?php echo $category["categoryCode"]; ?>">
                        <?php echo $category["categoryName"]; ?>
                        <a href="<?php echo $category["categoryName"]; ?>.php">vedi tutti</a>
                    </button>
                </h2>
            </div>
            
            <div id="collapse<?php echo $category["categoryCode"]; ?>" class="collapse" aria-labelledby="heading<?php echo $category["categoryCode"]; ?>">
                <div class="container-fluid">
                    <div class="row" id="rowCollapse">
                        <div class="col-xl-2">
                        </div>
                        
                        <?php foreach($dbh->getPopularEventsByCategory($category["categoryCode"]) as $event): ?>
                        <div class="col-md-6 col-lg-3 col-xl-2 event-card">
                            <div class="card event shadow">
                                <div class="card-image">
                                    <img src="<?php echo UPLOAD_DIR.$event["eventPic"];?>"  class="card-img-top img-fluid" alt="">
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $event["title"];?></h5>
                                    <p class="card-text"><?php echo $event["about"];?></p>
                                    <form action="event.php" method="POST">
                                        <input type="hidden" name="eventcode" value="<?php echo $event["eventCode"];?>">
                                        <label class="d-none" for="submit-dettagli-1">Dettagli</label><input type="submit" id="submit-dettagli-1" class="btn btn-primary" value="Dettagli">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <?php endforeach; ?>

    </div>