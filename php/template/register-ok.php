<div class="container">
    <div class="row mt-5">
        <div class="col-sm-12 text-center">
            <h2>Registrazione completata!</h2>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-sm-12 text-center">
            <?php if($_SESSION["user"]=="organizer"):?>
            <p>Il tuo account rimarrà inattivo fino a quando gli amministratori avranno confermato la tua identità.<br>
            Non potrai pertanto organizzare eventi, ma potrai comunque navigare sul sito; riceverai una notifica non appena verrai confermato.<br>
            Cliccando su "Conferma" invierai i tuoi dati agli amministratori che potranno darti, di seguito, il consenso.</p>    
            <?php endif;?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 offset-sm-10 text-right text-sm-left">
            <a href='index.php' class="ml-3 btn btn-primary" id="register-confirmed">Conferma</a>
        </div>
    </div>
    
</div>