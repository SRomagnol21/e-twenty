<div class="container-fluid">
        <div class="row">
            <div class="col-sm-2 align-self-center offset-sm-5">
                <img src="../img/logo2.png" alt="" id="loginLogo">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2 align-self-center offset-sm-5">
                <h1 id="loginHeader">Inizia</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 align-self-center offset-sm-4">
                <h2 id="loginSubheader">Per iniziare, accedi utilizzando il tuo username.</br>
                Se non hai ancora un account <a href="register.php">registrati adesso</a> per ottenere tutti i vantaggi!</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 align-self-center offset-sm-4">
                <?php if(isset($templateParams["errorelogin"])): ?>
                    <p class="text-danger"><?php echo $templateParams["errorelogin"]; ?></p>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 align-self-center offset-sm-4">
                <form action="login.php" method="POST" id="loginForm">
                    <label class="d-none" for="username">Username</label><input class="rounded" type="text" id="username" name="username" placeholder="username"/>
                    <label class="d-none" for="password">Password</label><input class="rounded" type="password" id="password" name="password" placeholder="password"/></br>
                    <label class="d-none" for="sumbit">Accedi</label><input class="btn btn-outline-primary" id="submit" type="submit" name="submit" value="Accedi">
                </form>
            </div>
        </div>
    </div>