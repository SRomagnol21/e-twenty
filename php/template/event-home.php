<div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 offset-sm-3">
        <div class="card shadow">
            <img src="<?php echo UPLOAD_DIR.$event["eventPic"];?>" class="card-img-top-event" alt="...">
            <div class="card-body">
                <h1><?php echo $event["title"];?></h1>
                <h2>Organizzato da <?php echo $event["userName"];?></h2>
                <hr>
                <ul class="p-2 bg-light shadow rounded" id="eventList">
                    <li class="m-2">
                        <i class="fa fa-calendar"></i>
                        <p class="text-danger">Data e ora</p><br>
                        <p class="ml-4"><?php echo $event["eventDate"]." ".$event["eventTime"];?></p>
                    </li>
                    <li class="m-2">
                        <i class="fa fa-location-arrow"></i>
                        <p class="text-danger">Luogo evento</p><br>
                        <p class="ml-4"><?php echo $event["placeName"].", ".$event["city"]." (".$event["province"].") ".$event["region"].", ".$event["country"];?></p>
                    </li>
                    <li class="m-2">
                        <i class="fa fa-file"></i>
                        <p class="text-danger">Descrizione</p></br>
                        <p class="ml-4"><?php echo $event["about"];?></p>
                    </li>
                </ul>
                <form action="cart.php" method="POST">
                    <ul class="p-2 container bg-light shadow rounded" id="eventList">
                        <li class="m-2">
                            <i class="fa fa-dollar"></i>
                            <p class="text-danger">Scegli la Categoria</p></br>
                        </li>
                        <li class="row mb-1 justify-content-center">
                            <p class="col-4 px-0">Categoria</p>
                            <p class="col-4 mx-0">Prezzo</p>
                            <p class="col-3 mx-0 px-0">Quantit&agrave;</p>
                        </li>
                        <?php foreach($slots as $slot): ?>
                            <?php $sold = $dbh->getSoldTicketsForEventAndSlot($event["eventCode"], $slot["slotCode"])[0]["COUNT(e.eventCode)"];?>
                            <?php if($slot["position"]!="" && $slot["maxLimit"]>=$sold): ?>
                                <li class="row mb-4 mx-0 justify-content-center bg-white shadow rounded" id="choose-category">
                                    <p class="col-4 mt-3"><?php echo $slot["position"];?></p>
                                    <input type="hidden" name="slotRequested<?php echo $slot["slotCode"];?>" value="<?php echo $slot["position"];?>">
                                    <p class="col-4 mt-3"><?php if($slot["price"]!=0): echo "€".$slot["price"]; else: echo "Gratis/Offerta Libera"; endif;?></p>
                                    <input type="hidden" name="price<?php echo $slot["slotCode"];?>" value="<?php echo $slot["price"];?>">
                                    <?php if($sold==$slot["maxLimit"]):?>
                                        <h4 class="text-danger col-3 px-0 mt-3">SOLD OUT</h4>
                                    <?php else:?>
                                        <label for="number-type<?php echo $slot["slotCode"];?>" class="d-none">Quantit&agrave;</label><input id="number-type<?php echo $slot["slotCode"];?>" class="col-3 px-0" type="number" name="quantity<?php echo $slot["slotCode"];?>" min="0" max="10" value="0">
                                    <?php endif;?>
                                </li>
                            <?php endif;?>
                        <?php endforeach;?>
                    </ul>
                    <?php if(!isset($_SESSION["username"])):?>
                        <input type="hidden" name="eventcode" value="<?php echo $event["eventCode"];?>">
                        <label for="addToCart" class="d-none">Aggiungi al carrello</label><input type="submit" id="addToCart" class="btn btn-primary float-right" value="Aggiungi al Carrello" disabled>
                    <?php else: if(isset($_SESSION["username"]) && $dbh->getUserType($_SESSION["username"])=="customer" && $event["deleted"] == 0):?>
                        <input type="hidden" name="eventcode" value="<?php echo $event["eventCode"];?>">
                        <label for="addToCart" class="d-none">Aggiungi al carrello</label><input type="submit" id="addToCart" class="btn btn-primary float-right" value="Aggiungi al Carrello" disabled>
                        <?php endif;?>
                    <?php endif;?>
                </form>
            </div>
        </div>
        </div>
    </dvi>
</div>
