<?php
require_once 'bootstrap.php';
$_SESSION["page"] = "login";

if($_SESSION["user"] == "admin"){
    $templateParams["nome"] = "login-organizer.php";
}
if(isset($_POST["org-events"])){
    $templateParams["eventi"] = $dbh->getEventsByAuthor($_POST["organizer"]);
    $templateParams["nome"] = "organizer-events.php";
}
if(isset($_POST["deactivate"])){
    $dbh->deactiveOrganizer($_POST["organizer"]);
}
if(isset($_POST["activate"])){
    $dbh->activeOrganizer($_POST["organizer"]);
}
require 'template/base.php';
?>