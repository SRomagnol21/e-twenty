<?php
require_once 'bootstrap.php';

$templateParams["titolo"] = "e20 - Eventi principali";
$_SESSION["page"] = "Eventi principali";

$templateParams["nome"] = "event-list.php";
$templateParams["events"] = $dbh->getAllPopularEvents();

require 'template/base.php';
?>