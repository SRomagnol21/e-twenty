<?php 

require_once 'bootstrap.php';

if(isset($_POST["option"])) {

    if($_POST["option"] != '') {
        
    }

    $dest = mysqli_real_escape_string($dbh->getConnection(), $_SESSION["username"]);
    if($_SESSION["user"]=="admin") {
        $query = "SELECT notificationcode, senderusername, shortdesc, about FROM notification, notification_sample WHERE destadmin = '$dest' AND criptokey = notificationkey AND notificationread = false";
    } else if($_SESSION["user"]=="organizer") {
        $query = "SELECT notificationcode, senderusername, shortdesc, about FROM notification, notification_sample WHERE destorganizer = '$dest' AND criptokey = notificationkey AND notificationread = false";
    } else if($_SESSION["user"]=="customer") {
        $query = "SELECT notificationcode, senderusername, shortdesc, about FROM notification, notification_sample WHERE destcustomer = '$dest' AND criptokey = notificationkey AND notificationread = false";
    }

    $result=mysqli_query($dbh->getConnection(), $query);
    $output='';
    $count = mysqli_num_rows($result);

    if($count > 0) {
        while($row = mysqli_fetch_array($result)) {
            $output .= "<li class='text-center'><p class='dropdown-notification'>
            <strong>".$row['shortdesc']."</strong><br><br>
            <input type='hidden' name='notificationcode' value=".$row['notificationcode'].">
            <input type='submit' name='submit' value='Vedi' class='btn btn-primary'></p>
            </li>
            <hr>";
        }
        $output .= "<li class='text-center dropdown-item-button'><a href='notification.php'>Vedi tutte</a></li>";
    } else {

        $output = "<li class='text-center'><p class='dropdown-item'>Non ci sono notifiche</p></li><hr>
            <li class='text-center dropdown-item-button'><a href='notification.php'>Vedi tutte</a></li>";
    }

    $data = array(

        'notification' => $output,
        'countNotification' => $count

    );

    echo json_encode($data);

}

?>