<?php 

    require 'bootstrap.php';

    $count = 0;
    $items = $cart->getItems();
    foreach($items as $item):
        foreach($item as $slot):
            $count++;
        endforeach;
    endforeach;
    
    echo json_encode($count);

?>