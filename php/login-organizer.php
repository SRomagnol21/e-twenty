<div class="container">
    <div class="row title">
        <div class="col-12">
            <h2>Lista Organizzatori</h2>
        </div>
    </div>
    <?php if(isAdminLoggedIn()): $templateParams["organizzatori"] = $dbh->getAllOrganizers(); endif; ?>
    <?php foreach($templateParams["organizzatori"] as $organizer): ?>
        <form action="login-organizerlist.php" method="POST">
            <div class="row rounded item">
                <div class="col-12 col-sm-6 text-left" id="organizerUsername">
                    <p><?php echo $organizer["username"] ?></p>
                </div>
                <div class="col-12 col-sm-6 text-right" id="organizerActive">
                    <input type="hidden" name="organizer" value=<?php echo $organizer["username"]?>></input>
                    <label for="org-events" class="d-none">Vedi eventi</label>
                    <input type="submit" name="org-events" id="org-events" value="vedi eventi" class="btn btn-primary"></input>
                    <label for="<?php if($organizer["active"]==1){ echo 'deactivate';} else {echo 'activate';} ?>" class="d-none"><?php if($organizer["active"]==1){ echo 'Disattiva';} else {echo 'Attiva';} ?></label>
                    <input type="submit" <?php if($organizer["active"]==1){ echo ' name="deactivate" id="deactivate" value="disattiva" ';} else{ echo ' name="activate" id="activate" value="attiva" ';}?> class="btn btn-primary"></input>
                </div>
            </div>
        </form>
    <?php endforeach; ?>
</div>