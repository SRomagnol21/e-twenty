<?php 
require_once 'bootstrap.php';

if(isset($_GET["buy"])) {
    
    $templateParams["nome"] = "tickets-bought.php";
    $items = $cart->getItems();
    $e = 0;
    $ntickets = $cart->getTicketQuantity();
    foreach($items as $evento):
        $s = 0;
        $slots = array_keys($evento);
        foreach($evento as $slot):
            $eventcode = array_keys($items)[$e];
            $slotcode = $dbh->getSlotCodeByName($eventcode, $slots[$s])[0]["slotCode"];
            for($i=0; $i < $slot["quantity"]; $i++) {
                $dbh->insertTicket($eventcode, $slotcode, $_SESSION["username"]);
            }
            $sold = $dbh->getSoldTicketsForEventAndSlot($eventcode, $slotcode)[0]["COUNT(e.eventCode)"];
            $limit = $dbh->getSlotByCode($eventcode, $slotcode)[0]["maxLimit"];
            if($sold == $limit) {
                $_SESSION["dest"] = "organizer";
                $_SESSION["destusername"] = $dbh->getEventById($eventcode)[0]["userName"];
                $_SESSION["notifica"] = "soldout";
            }
            $s++;
        endforeach;
        $e++;
    endforeach;

    $cart->clear();

} else {

    $templateParams["nome"] = "tickets-home.php";
}
require 'template/base.php';
?>