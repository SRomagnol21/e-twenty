<?php
require_once "bootstrap.php";

if(isUserLoggedIn() && (!isOrganizerLoggedIn() && !isAdminLoggedIn())){
    echo("Solo gli organizzatori possono gestire eventi!");
} else {
    if(!isUserLoggedIn() || !isset($_GET["action"]) || ($_GET["action"]!=1 && $_GET["action"]!=2 && $_GET["action"]!=3) || ($_GET["action"]!=1  && !isset($_GET["id"]))){
        header("location: login.php");
    }
}

if($_GET["action"]!=1){
    $result = $dbh->getEventById($_GET["id"]);
    if(count($result) == 0){
        $templateParams["event"] = null;
        $templateParams["slots"] = null;
    } else {
        $templateParams["event"] = $result[0];
        $templateParams["slots"] = getEmptyPriceSlots();
        $priceSlots = $dbh->getPriceSlotsByEventId($_GET["id"]);
        if(count($priceSlots)>=1) {
            $templateParams["slots"][0] = $priceSlots[0];
        }
        if(count($priceSlots)>=2) {
            $templateParams["slots"][1] = $priceSlots[1];
        }
        if(count($priceSlots)>=3) {
            $templateParams["slots"][2] = $priceSlots[2];
        }
    }
    $_SESSION["dest"]="customer";
    if($_GET["action"]==2){
        $_SESSION["notifica"]="modifica";
    } else {
        $_SESSION["notifica"]="elimina";
    }
    $_SESSION["eventoModificato"]=$_GET["id"];
} else {
    $templateParams["event"] = getEmptyEvent();
    $templateParams["slots"] = getEmptyPriceSlots();
}

$templateParams["titolo"] = "e20 - Gestisci Eventi";
$templateParams["nome"] = "organizer-form.php";
$templateParams["categories"] = $dbh->getCategories();
$templateParams["action"] = $_GET["action"];

require "template/base.php";
?>