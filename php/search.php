<?php
require_once 'bootstrap.php';

$templateParams["titolo"] = "e20 - Risultati ricerca";
$_SESSION["page"] = "search";

if(isset($_POST["search"])) {
    $templateParams["nome"] = "event-list.php";
    $templateParams["events"] = $dbh->searchEvents($_POST["search"]);
}

require 'template/base.php';
?>