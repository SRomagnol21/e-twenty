<?php
require_once 'bootstrap.php';

$templateParams["titolo"] = "e20 - Cinema";
$_SESSION["page"] = "Cinema";

$templateParams["nome"] = "event-list.php";
$templateParams["events"] = $dbh->getAllEventsFromCategory($_SESSION["page"]);

require 'template/base.php';
?>