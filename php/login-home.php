<?php
require_once 'bootstrap.php';
$_SESSION["page"] = "login";

if($_SESSION["user"] == "organizer"){
    $templateParams["nome"] = "login-events.php";
    $templateParams["eventi"] = $dbh->getEventsByAuthor($_SESSION["username"]);
}
if($_SESSION["user"] == "admin"){
    $templateParams["nome"] = "login-events.php";
}
if(isset($_GET["formmsg"])){
    $templateParams["formmsg"] = $_GET["formmsg"];
} else {
    $templateParams["formmsg"] = "";
}
require 'template/base.php';
?>