<?php
require_once 'bootstrap.php';

$templateParams["titolo"] = "e20 - Concerti";
$_SESSION["page"] = "Concerti";

$templateParams["nome"] = "event-list.php";
$templateParams["events"] = $dbh->getAllEventsFromCategory($_SESSION["page"]);

require 'template/base.php';
?>