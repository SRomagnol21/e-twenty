<?php

    $tickets = $dbh->getLastnTickets($ntickets, $_SESSION["username"]);

?>
<div class="container">
    <div class="row">
        <div class="col-12 title">
            <h2 class="text-danger">Acquisto effettuato con successo!</h2>
        </div>
    </div>
    <div class="row mb-5" id="empty-cart">
        <div class="col-sm-8 offset-sm-2 text-center">
        <p>Grazie per il tuo acquisto.
            <br>Per poter accedere fisicamente all'evento sarà richiesto di esibire il biglietto e un documento.
            <br>L'intestatario dei biglietti <b>deve</b> essere presente in caso di biglietti comprati per terzi.</p>
        <hr>
        <p>Stampa qui i tuoi biglietti o vai alla pagina <a href="tickets.php">I Miei Biglietti</a>.</p>
        </div>
    </div>
    <?php foreach ($tickets as $ticket) :
        $event = $dbh->getEventById($ticket["eventcode"])[0];
        $slot = $dbh->getSlotByCode($ticket["eventcode"], $ticket["slotcode"])[0]; ?>
        <div class="card shadow my-4" id="<?php echo $event["eventCode"];?>">
            <h5 class="card-header align-middle py-3">
                <?php echo $event["title"] ?> - <?php echo $slot["position"] ?> - #<?php echo $ticket["ticketcode"]; ?>
            </h5>
            <div class="card-body p-3">
                <h5 class="card-title"><?php echo $event["userName"] ?></h5>
                <div class="row justify-content-end">
                    <div class="col-10 col-sm-11">
                    <p class="card-text"><?php echo $event["eventDate"] ?></br><?php echo $event["eventTime"] ?></p>
                    </div>
                    <div class="col-2 col-sm-1 text-left align-bottom py-0 px-0">
                        <button class="download-btn btn btn-outline-primary" value="<?php echo $ticket["ticketcode"]; ?>">
                            <span class="fa fa-download"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>