<?php 
if(!isset($_GET["max"])){
    $max = 3;
} else {
    $max = intval($_GET["max"]);
}
$n=$dbh->getAllMyNotifications($_SESSION["username"], $_SESSION["user"]);
$maxlen=count($n);
?>

<form action="notification.php" method="POST">
    <div class="container">
        <div class="row title">
            <div class="col-sm-4 offset-sm-4">
                <h2>Notifiche</h2>
            </div>
        </div>
    <?php 
        if($maxlen > $max) {
            $maxlen = $max;
        }
        for($i=0;$i<$maxlen;$i++):?>
    
        
        <div class="card shadow my-4" id="<?php echo $n[$i]["notificationcode"];?>">
            <h5 class="card-header align-middle py-3">
                <label for="<?php echo $n[$i]["notificationcode"]?>" class="d-none">Seleziona notifica</label><input type="checkbox" class="checkbox mr-3" id="<?php echo $n[$i]["notificationcode"]?>"> <?php echo $n[$i]["senderusername"];?>
            </h5>
            <div class="card-body p-3">
                <h5 class="card-title"><?php echo $n[$i]["shortdesc"];?></h5>
                
                <div class="row justify-content-end">
                    <div class="col-5">
                    <p class="card-text"><?php echo $n[$i]["notificationdate"];?></br><?php echo $n[$i]["notificationtime"];?></p>
                    </div>
                    <div class="col-7 text-right align-middle py-2">
                        <form action="notification.php" method="POST">
                            <input type='hidden' name='notificationcode' value="<?php echo $n[$i]["notificationcode"];?>">
                            <label for="submit1" class="d-none">Vedi notifica</label><input type="submit" id="submit1" name="submit" value="Vedi notifica" class="btn btn<?php if($n[$i]["notificationread"]):?>-outline<?php endif;?>-primary notification-button px-3 py-2">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php endfor; ?>
    </div>
</form>

<div class="container" style="margin-top: 20px;">
    <div class="row justify-content-end">
        <div class="col-4">
            <form action="notification.php" method="GET">
                <input type="hidden" name="max" value="<?php echo $max+3;?>">
                <label for="expand" class="d-none">Espandi</label><input type="submit" id="expand" style="border: none; background: transparent;" value="Espandi">
            </form>
        </div>
        <div class="col-8 text-right">
            <label for="read-notifications" class="d-none">Segna come lette</label><button class="btn btn-primary" id="read-notifications">Segna come gi&agrave; letti</button>
        </div>
    </div>
</div>
