$(document).ready(function(){

    setInterval(function() {
        $.ajax({

            url: 'fetch.php',
            method: 'POST',
            data: {option: ''},
            dataType: 'json',
            success: function(data) {
                $("#dropdown-menu-id").html(data.notification);
                $("#dropdown-menu-id-2").html(data.notification);

                if(data.countNotification > 0) {
                    $("#bell-count").html(data.countNotification);
                    $("#bell-count-2").html(data.countNotification);
                }
            }
        });

        $.ajax({
            url: 'cart-count.php',
            dataType: 'json',
            success: function(count) {
                if(count > 0) {
                    $("#cart-count").html(count);
                    $("#cart-count-2").html(count);
                }
            }
        });

    }, 1000);

    function submitNotification(){
        $.ajax({ url: 'submit.php' });
    }

    $("#register-confirmed", "#sold-out", "#modify", "#delete").on("click", function(){
        submitNotification();
    });

    $("#deny-organizer", "#confirm-organizer").on("click", function(){
        setTimeout(submitNotification, 20);     
    });

    $("#read-notifications").on("click", function(){
        var cb = $(".checkbox");
        var ids = Array();
        for(i=0;i<cb.length;i++){
            if(cb[i].checked) {
                ids.push(cb[i].id);
            }
        }
        if(ids.length>0){
            for(i=0;i<ids.length;i++){
                var id = ids[i];
                $.ajax({
                    url: 'readNotifications.php',
                    data: {val: id},
                    success: function() {
                        location.reload();
                    }
                });
            }
        }
    });

    $(".not-showing").hide();

    $("#fa-plus").on("click", function(){
        if($("#li2").hasClass("not-showing")) {
            $("#li2").show();
            $("#li2").removeClass("not-showing");
        } else if($("#li3").hasClass("not-showing")) {
            $("#li3").show();
            $("#li3").removeClass("not-showing");
        }
    });

    $("#fa-minus").on("click", function(){
        if(!$("#li3").hasClass("not-showing")) {
            $("#li3").hide();
            $("#li3").addClass("not-showing");
        } else if(!$("#li2").hasClass("not-showing")) {
            $("#li2").hide();
            $("#li2").addClass("not-showing");
        }
    });

    $("#number-type1,#number-type2,#number-type3").on("click", function(){
        if($("#number-type1").val() > 0 || $("#number-type2").val() > 0 || $("#number-type3").val() > 0){
            document.getElementById("addToCart").style.background = "#b30000";
            document.getElementById("addToCart").disabled = false;
        } else {
            document.getElementById("addToCart").style.background = "grey";
            document.getElementById("addToCart").disabled = true;
        }
    });

    $(".download-btn").on("click", function(){
        $.ajax({
            url: "tickets-image.php",
            method: "POST",
            data: {ticket: $(this).val()},
            dataType: "json",
            success: function(data) {
                console.log("download");
                var download = document.createElement('a');
                download.setAttribute('href', 'data:image/jpeg; base64,' + data.img);
                download.setAttribute('download', 'ticket'+ data.code +'.jpg');

                download.style.display = 'none';
                document.body.appendChild(download);
                download.click();
                document.body.removeChild(download);
            }
        });
    });

    $(".place").on("click", function(){
        var name = $(this).html();
        $("#placeSpaceInput").val(name);
        $("#placeSpaceBtn").html(name);
    });

    $("#addPlace").on("click", function(){
        var place = Array($("#newCountry").val(), $("#newRegion").val(), $("#newProvince").val(), $("#newCity").val(), $("#newPlaceName").val());
        $.ajax({
            url: "addPlace.php",
            method: "POST",
            data: {place: place},
            dataType: "json",
            success: function(name) {
                $("#allPlaces").append("<li class='place'>"+ name +"</li>");
                $(".hidePlaces").hide();
            }
        });
    });

    $("#showNewPlace").on("click", function(){
        if($(".hidePlaces").hasClass("not-showing")) {
            $(".hidePlaces").removeClass("not-showing");
            $(".hidePlaces").show();
        }
    });

    $("#hideNewPlace").on("click", function(){
        if(!$(".hidePlaces").hasClass("not-showing")) {
            $(".hidePlaces").addClass("not-showing");
            $(".hidePlaces").hide();
        }
    });

});