<div class="container">
    <div class="row title">
        <div class="col-sm-4 offset-sm-4">
            <h2>Dati notifica</h2>
        </div>
    </div>
    <div class="row info">
        <div class="col-6">
            <p>Username:</p>
        </div>
        <div class="col-6">
            <p><?php echo $templateParams["senderdata"][0]["username"]; ?></p>
        </div>
    </div>
    <div class="row info">
        <div class="col-6">
            <p>Nome:</p>
        </div>
        <div class="col-6">
            <p><?php echo $templateParams["senderdata"][0]["firstname"]; ?></p>
        </div>
    </div>
    <div class="row info">
        <div class="col-6">
            <p>Cognome:</p>
        </div>
        <div class="col-6">
            <p><?php echo $templateParams["senderdata"][0]["lastname"]; ?></p>
        </div>
    </div>
    <div class="row info">
        <div class="col-6">
            <p>Data di nascita:</p>
        </div>
        <div class="col-6">
            <p><?php echo $templateParams["senderdata"][0]["birthdate"]; ?></p>
        </div>
    </div>
    <div class="row info">
        <div class="col-6">
            <p>Paese:</p>
        </div>
        <div class="col-6">
            <p><?php echo $templateParams["senderdata"][0]["country"]; ?></p>
        </div>
    </div>
    <div class="row info">
        <div class="col-6">
            <p>Regione:</p>
        </div>
        <div class="col-6">
            <p><?php echo $templateParams["senderdata"][0]["region"]; ?></p>
        </div>
    </div>
    <div class="row info">
        <div class="col-6">
            <p>Città</p>
        </div>
        <div class="col-6">
            <p><?php echo $templateParams["senderdata"][0]["city"]; ?></p>
        </div>
    </div>
    <div class="row info">
        <div class="col-sm-12">
            <p><strong>L'utente <?php echo $dbh->getNotification($_POST["notificationcode"])[0]["about"]; ?></strong></p>
        </div>
    </div>
    <form action="notification.php" method="POST">
        <div class="row accept">
            <div class="col-12 col-sm-6 offset-sm-6">
            <?php if($templateParams["senderdata"][0]["active"] == 0):?>
                <label for="confirm-organizer" class="d-none">Conferma organizzatore</label><input type="submit" name="response" id="confirm-organizer" class="btn btn-primary" value="Conferma utente" style="margin-right: 10px;"></input>
                <label for="deny-organizer" class="d-none">Rifiuta organizzatore</label><input type="submit" name="response" id="deny-organizer" class="btn btn-primary" value="Nega utente"></input>
            <?php endif; ?>
            </div>
        </div>
    </form>
</div>