<div class="container">
    <div class="row">
        <div class="col-12" id="organizerTitle">
            <?php if(isAdminLoggedIn()):?>
            <h2>Eventi di <?php echo $_POST["organizer"]; ?></h2>
            <?php endif;?>
        </div>
    </div>
    <?php foreach($templateParams["eventi"] as $event): ?>
        <div class="row rounded item">
            <div class="col-xs-10 align-self-center" id="organizerEventTitle">
                <p><?php echo $event["title"] ?></p>
            </div>
            <div class="col-1 align-self-left" id="organizerButton">
                <a href="manage-event.php?action=2&id=<?php echo $event["eventCode"]; ?>" id="modifica"><i class="fa fa-edit"></i></a>
            </div>
            <div class="col-1 align-self-right" id="organizerButton">
                <a href="manage-event.php?action=3&id=<?php echo $event["eventCode"]; ?>" id="elimina"><i class="fa fa-trash"></i></a>
            </div>
        </div>
    <?php endforeach; ?>
</div>