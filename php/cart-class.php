<?php
class Cart
{
	/*Quantity of items that a cart can support. Zero means infinite.*/
	private $maxItems = 0;

	/*Limit of ticket that can be bought in an order. Zero means infinite.*/
	private $itemLimit = 0;
	
	private $items = [];
	
	public function __construct($options = []){
		if (isset($options['maxItems'])) {
			$this->maxItems = $options['maxItems'];
		}
		if (isset($options['itemLimit'])) {
			$this->itemLimit = $options['itemLimit'];
		}
		
		$this->read();
	}
	
	public function getItems(){
		return $this->items;
	}
	
	public function isEmpty(){
		return empty(array_filter($this->items));
	}
	
	public function getEventQuantity(){
		$total = 0;
		foreach ($this->items as $items) {
			foreach ($items as $item) {
				++$total;
			}
		}
		return $total;
	}
	
	public function getTicketQuantity(){
		$quantity = 0;
		foreach ($this->items as $items) {
			foreach ($items as $item) {
				$quantity += $item['quantity'];
			}
		}
		return $quantity;
	}
	
	public function getTotalPrice(){
		$total = 0;
		foreach ($this->items as $slot) {
			foreach($slot as $item) {
				if (isset($item['price'])) {
					$total += $item['price'] * $item['quantity'];
				}
			}
		}
		return $total;
	}
	
	public function clear(){
		$this->items = [];
		$this->write();
	}
	
	public function add($id, $slot, $quantity, $price){
		if (count($this->items) >= $this->maxItems && $this->maxItems != 0) {
			return false;
		}
		
		if (isset($this->items[$id][$slot])) {
			$this->items[$id][$slot]['quantity'] += $quantity;
			$this->write();
			return true;		
		}
	
		$this->items[$id][$slot]["quantity"] = ($quantity > $this->itemLimit && $this->itemLimit != 0) ? $this->itemLimit : $quantity;
		$this->items[$id][$slot]["price"] = $price;
		$this->write();
		return true;
	}
	
	public function update($id, $slot, $quantity){
		if ($quantity == 0) {
			$this->remove($id, $slot);
			$this->write();
			return true;
		}
		if (isset($this->items[$id][$slot])) {
			$this->items[$id][$slot]["quantity"] = ($quantity > $this->itemLimit && $this->itemLimit != 0) ? $this->itemLimit : $quantity;
			$this->write();
			return true;
		}
		return false;
	}
	
	public function remove($id, $slot){
		if (!isset($this->items[$id][$slot])) {
			return false;
		}
		
		unset($this->items[$id][$slot]);
		$this->write();
		return true;
	}
	
	public function destroy(){
		$this->items = [];
		unset($_SESSION["cart"]);
	}
	
	private function read(){
		$this->items = json_decode((isset($_SESSION["cart"])) ? $_SESSION["cart"] : '[]', true);
	}
	
	private function write(){
		$_SESSION["cart"] = json_encode(array_filter($this->items));	
	}
}