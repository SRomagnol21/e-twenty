create table ADMIN (
     FirstName char(20) not null,
     LastName char(20) not null,
     UserName char(20) not null,
     Email char(40) not null,
     UserPassword char(255) not null,
     Salt char(255) not null,
     City char(20) not null,
     Province char(2) not null,
     Region char(20) not null,
     Country char(20) not null,
     BirthDate date not null,
     constraint IDADMIN primary key (UserName));

create table CATEGORY (
     CategoryCode int not null AUTO_INCREMENT,
     CategoryName char(20) not null,
     CategoryDescription char(255) not null,
     constraint IDCATEGORY primary key (CategoryCode));

create table CUSTOMER (
     FirstName char(20) not null,
     LastName char(20) not null,
     UserName char(20) not null,
     Email char(40) not null,
     UserPassword char(255) not null,
     Salt char(255) not null,
     City char(20) not null,
     Province char(2) not null,
     Region char(20) not null,
     Country char(20) not null,
     BirthDate date not null,
     constraint IDCUSTOMER primary key (UserName));

create table EVENTS (
     EventCode int not null AUTO_INCREMENT,
     Title char(20) not null,
     EventDate date not null,
     EventTime char(10) not null,
     About char(255) not null,
     CategoryCode int not null,
     PlaceCode int not null,
     UserName char(20) not null,
     EventPic char(255) not null,
     constraint IDEVENTO_ID primary key (EventCode));

create table ORGANIZER (
     FirstName char(20) not null,
     LastName char(20) not null,
     UserName char(20) not null,
     Email char(40) not null,
     UserPassword char(255) not null,
     Salt char(255) not null,
     City char(20) not null,
     Province char(2) not null,
     Region char(20) not null,
     Country char(20) not null,
     BirthDate date not null,
     Active boolean not null,
     constraint IDORGANIZER primary key (UserName));

create table PLACE (
     PlaceCode int not null AUTO_INCREMENT,
     PlaceName char(20) not null,
     City char(20) not null,
     Province char(2) not null,
     Region char(20) not null,
     Country char(20) not null,
     constraint IDLUOGO primary key (PlaceCode));

create table PRICE_SLOT (
     EventCode int not null,
     SlotCode int not null,
     Position char(20) not null,
     Price int not null,
     MaxLimit int not null,
     constraint IDLISTINO primary key (EventCode, SlotCode));

create table TICKET (
     EventCode int not null,
     SlotCode int not null,
     TicketCode int not null AUTO_INCREMENT,
     UserName char(20) not null,
     constraint IDBIGLIETTO primary key (TicketCode));



alter table EVENTS add constraint FKbelonging
     foreign key (CategoryCode)
     references CATEGORY;

alter table EVENTS add constraint FKin
     foreign key (PlaceCode)
     references PLACE;

alter table EVENTS add constraint FKorganization
     foreign key (UserName)
     references ORGANIZER;

alter table PRICE_SLOT add constraint FKcatalogue
     foreign key (EventCode)
     references EVENT;

alter table TICKET add constraint FKpurchase
     foreign key (UserName)
     references CUSTOMER;

alter table TICKET add constraint FKgeneration
     foreign key (EventCode, SlotCode)
     references PRICE_SLOT;

