create table notification_sample ( NotificationKey char(30) not null, About char(255) not null, ShortDesc char(255) not null, constraint IDNOTIFICATIONKEY primary key (NotificationKey));

create table notification ( NotificationCode int AUTO_INCREMENT not null, CriptoKey char(255) not null, DestCustomer char(30), DestOrganizer char(30), DestAdmin char(30), SenderUsername char(30), NotificationRead boolean not null, NotificationDate date not null, NotificationTime time not null, EventCode int, constraint IDNOTIFICATION primary key (NotificationCode));
