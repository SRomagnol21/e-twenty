-- *********************************************
-- * Standard SQL generation                   
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Thu Dec  5 15:28:04 2019 
-- * LUN file: C:\Users\simon\Desktop\Università\Tecnologie Web\e20\e20db.lun 
-- * Schema: e20/1 
-- ********************************************* 


-- Database Section
-- ________________ 

-- DBSpace Section
-- _______________


-- Tables Section
-- _____________ 

create table ADMIN (
     FirstName char(20) not null,
     LastName char(20) not null,
     UserName char(20) not null,
     Email char(40) not null,
     UserPassword char(20) not null,
     City char(20) not null,
     Province char(2) not null,
     Region char(20) not null,
     Country char(20) not null,
     BirthDate date not null,
     constraint IDADMIN primary key (UserName));

create table CATEGORY (
     CategoryCode int identity not null,
     CategoryName char(20) not null,
     CategoryDescription char(1000) not null,
     constraint IDCATEGORY primary key (CategoryCode));

create table CUSTOMER (
     FirstName char(20) not null,
     LastName char(20) not null,
     UserName char(20) not null,
     Email char(40) not null,
     UserPassword char(20) not null,
     City char(20) not null,
     Province char(2) not null,
     Region char(20) not null,
     Country char(20) not null,
     BirthDate date not null,
     constraint IDCUSTOMER primary key (UserName));

create table EVENT (
     EventCode int identity not null,
     Title char(20) not null,
     EventDate date not null,
     EventTime char(10) not null,
     About char(1000) not null,
     CategoryCode int not null,
     PlaceCode int not null,
     UserName char(20) not null,
     constraint IDEVENTO_ID primary key (EventCode));

create table ORGANIZER (
     FirstName char(20) not null,
     LastName char(20) not null,
     UserName char(20) not null,
     Email char(40) not null,
     UserPassword char(20) not null,
     City char(20) not null,
     Province char(2) not null,
     Region char(20) not null,
     Country char(20) not null,
     BirthDate date not null,
     constraint IDORGANIZER primary key (UserName));

create table PLACE (
     PlaceCode int identity not null,
     PlaceName char(20) not null,
     City char(20) not null,
     Province char(2) not null,
     Region char(20) not null,
     Country char(20) not null,
     constraint IDLUOGO primary key (PlaceCode));

create table PRICE_SLOT (
     EventCode int not null,
     SlotCode int identity not null,
     Position char(20) not null,
     Price int not null,
     constraint IDLISTINO primary key (EventCode, SlotCode));

create table TICKET (
     EventCode int not null,
     SlotCode int not null,
     TicketCode int identity not null,
     UserName char(20) not null,
     constraint IDBIGLIETTO primary key (EventCode, SlotCode, TicketCode));


-- Constraints Section
-- ___________________ 

alter table EVENT add constraint FKbelonging
     foreign key (CategoryCode)
     references CATEGORY;

alter table EVENT add constraint FKin
     foreign key (PlaceCode)
     references PLACE;

alter table EVENT add constraint FKorganization
     foreign key (UserName)
     references ORGANIZER;

alter table PRICE_SLOT add constraint FKcatalogue
     foreign key (EventCode)
     references EVENT;

alter table TICKET add constraint FKpurchase
     foreign key (UserName)
     references CUSTOMER;

alter table TICKET add constraint FKgeneration
     foreign key (EventCode, SlotCode)
     references PRICE_SLOT;


-- Index Section
-- _____________ 

